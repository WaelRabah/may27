import React, { PureComponent } from 'react';
import { GaContext } from '../../googleAnalyticsContext/GaContext';
export default class PageNotFound extends PureComponent {
  static contextType =GaContext
  componentDidMount()
  {
    const {ReactGA} = this.context
    ReactGA.pageview('/pageNotFound')
  }
  render() {
    return (
      <div className="common-page-not-found">
        Page not found.
      </div>
    );
  }
}
