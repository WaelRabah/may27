import {
  RETRIEVE_SOURCES_FILTER_BEGIN,
  RETRIEVE_SOURCES_FILTER_SUCCESS,
  RETRIEVE_SOURCES_FILTER_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveSourcesFilter(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_SOURCES_FILTER_BEGIN,
    });
    const url = `${apiEndPoints.sources.RETRIEVE_SOURCES}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_SOURCES_FILTER_SUCCESS,
            data: res.data.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_SOURCES_FILTER_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_SOURCES_FILTER_BEGIN:
      return {
        ...state,
        retrieveTopicsdropdownBegin: true,
        retrieveTopicsdropdownSuccess: false,
        retrieveTopicsdropdownError: false,
        allSources: [],
      };
    case RETRIEVE_SOURCES_FILTER_SUCCESS:
      return {
        ...state,
        retrieveTopicsdropdownBegin: false,
        retrieveTopicsdropdownSuccess: true,
        allSources: action.data,
      };
    case RETRIEVE_SOURCES_FILTER_FAILURE:
      return {
        ...state,
        retrieveTopicsdropdownBegin: false,
        retrieveTopicsdropdownError: true,
      };

    default:
      return state;
  }
}
