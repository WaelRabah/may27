import { TOGGLE_ACTIVE_TAB } from './constants';

export function toggleTabs(data) {
  return {
    type: TOGGLE_ACTIVE_TAB,
    res: data,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case TOGGLE_ACTIVE_TAB:
      return {
        ...state,
        activeTab: action.res,
      };

    default:
      return state;
  }
}
