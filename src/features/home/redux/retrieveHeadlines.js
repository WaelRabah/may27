import {
  RETRIEVE_HEADLINES_BEGIN,
  RETRIEVE_HEADLINES_SUCCESS,
  RETRIEVE_HEADLINES_FAILURE,
  FILTER_RETRIEVE_HEADLINES_SUCCESS,
  FILTER_RETRIEVE_HEADLINES_BEGIN,
  FILTER_RETRIEVE_HEADLINES_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveAllHeadlines(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_HEADLINES_BEGIN,
    });
    const url = `${apiEndPoints.headlines.RETRIEVE_HEADLINES}?fromDate=${props.fromDate}&toDate=${
      props.toDate
    }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sentiment=${
      props.sentiments && props.sentiments.length ? props.sentiments.join(',').toLowerCase() : 'all'
    }&sources=${
      props.sources && props.sources.length ? props.sources.join(',').toLowerCase() : 'all sources'
    }`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_HEADLINES_SUCCESS,
            data:
              res.status === 200
                ? { count: res.data.count, data: res.data.data }
                : { count: null, data: [] },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_HEADLINES_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}
export function filterAllHeadlines(props = {}) {
  return dispatch => {
    dispatch({
      type: FILTER_RETRIEVE_HEADLINES_BEGIN,
    });
    const url = `${apiEndPoints.headlines.RETRIEVE_HEADLINES}?fromDate=${props.fromDate}&toDate=${
      props.toDate
    }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sentiment=${
      props.sentiments && props.sentiments.length ? props.sentiments.join(',').toLowerCase() : 'all'
    }&sources=${
      props.sources && props.sources.length ? props.sources.join(',').toLowerCase() : 'all sources'
    }`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: FILTER_RETRIEVE_HEADLINES_SUCCESS,
            data:
              res.status === 200
                ? { count: res.data.count, data: res.data.data }
                : { count: null, data: [] },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: FILTER_RETRIEVE_HEADLINES_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_HEADLINES_BEGIN:
      return {
        ...state,
        retrieveHeadlinesBegin: true,
        retrieveHeadlinesSuccess: false,
        retrieveHeadlinesFailure: false,
      };
    case RETRIEVE_HEADLINES_SUCCESS:
      function headlinesStateAlreadyContainsThisReceivedData() {
        if (JSON.stringify(state.allHeadlines.data).includes(JSON.stringify(action.data.data).slice(0,-1))) {
          return true;
        }
        else {
          return false;
        }
      }
      let updatedHeadlinesData = [];
      if (state.allHeadlines.data && action.data.data && 
          (JSON.stringify(state.allHeadlines.data) !== JSON.stringify(action.data.data)) &&
          (!headlinesStateAlreadyContainsThisReceivedData())
        ) {
        updatedHeadlinesData = [...state.allHeadlines.data, ...action.data.data];
      } else if (action.data.data) {
        updatedHeadlinesData = action.data.data;
      }
      return {
        ...state,
        retrieveHeadlinesBegin: false,
        retrieveHeadlinesSuccess: true,
        allHeadlines: { data: updatedHeadlinesData, count: action.data.count },
      };
    case RETRIEVE_HEADLINES_FAILURE:
      return {
        ...state,
        retrieveHeadlinesBegin: false,
        retrieveHeadlinesFailure: true,
      };
    case FILTER_RETRIEVE_HEADLINES_BEGIN:
      return {
        ...state,
        retrieveHeadlinesBegin: true,
        retrieveHeadlinesSuccess: false,
        allHeadlines: {},
      };
    case FILTER_RETRIEVE_HEADLINES_SUCCESS:
      return {
        ...state,
        retrieveHeadlinesBegin: false,
        retrieveHeadlinesSuccess: true,
        allHeadlines: { data: action.data.data, count: action.data.count },
      };
    case FILTER_RETRIEVE_HEADLINES_FAILURE:
      return {
        ...state,
        retrieveHeadlinesBegin: false,
        retrieveHeadlinesFailure: true,
      };
    default:
      return state;
  }
}