import {
  UPDATE_COMMENTS_RELEVANT_BEGIN,
  UPDATE_COMMENTS_RELEVANT_SUCCESS,
  UPDATE_COMMENTS_RELEVANT_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateCommentsRelevant(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_COMMENTS_RELEVANT_BEGIN,
    });
    const url = `${apiEndPoints.comments.UPDATE_COMMENT_RELEVANT}?commentId=${props.commentId}&relevance=${props.relevance}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_COMMENTS_RELEVANT_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_COMMENTS_RELEVANT_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_COMMENTS_RELEVANT_BEGIN:
      return {
        ...state,
        updateCommentsRelevantBegin: true,
        updateCommentsRelevantSuccess: false,
        updateFeedsRelevantSuccess: false,
        updateCommentsRelevantFailure: false,
      };
    case UPDATE_COMMENTS_RELEVANT_SUCCESS:
      const updatedComments = state.allComments.data.map(comment => {
        if (comment.commentId === action.data.commentId) {
          comment.relevance = action.data.relevance;
        }
        return comment;
      });
      state.allComments.data = updatedComments;
      return {
        ...state,
        updateCommentsRelevantBegin: false,
        updateCommentsRelevantSuccess: true,
      };
    case UPDATE_COMMENTS_RELEVANT_FAILURE:
      return {
        ...state,
        updateCommentsRelevantBegin: false,
        updateCommentsRelevantFailure: true,
      };
    default:
      return state;
  }
}
