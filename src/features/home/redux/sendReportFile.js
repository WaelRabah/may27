import {
  SEND_SHARE_REPORT_BEGIN,
  SEND_SHARE_REPORT_SUCCESS,
  SEND_SHARE_REPORT_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function createShareReportFile(props = {}) {
  return dispatch => {
    dispatch({
      type: SEND_SHARE_REPORT_BEGIN,
    });
    const url = `${apiEndPoints.shareReport.SEND_SHARE_REPORT}`;
    const promise = new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append('email', props.email);
      formData.append('topic_id',props.topic_id);
      formData.append('topic_name',props.topic_name);
      formData.append('to_date',props.to_date);
      formData.append('from_date',props.from_date);
      const doRequest = http.post(url, formData);
      doRequest.then(
        res => {
          dispatch({
            type: SEND_SHARE_REPORT_SUCCESS,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: SEND_SHARE_REPORT_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case SEND_SHARE_REPORT_BEGIN:
      return {
        ...state,
        sendShareReportBegin: true,
        sendShareReportSuccess: false,
        sendShareReportError: false,
      };
    case SEND_SHARE_REPORT_SUCCESS:
      return {
        ...state,
        sendShareReportBegin: false,
        sendShareReportSuccess: true,
      };
    case SEND_SHARE_REPORT_FAILURE:
      return {
        ...state,
        sendShareReportBegin: false,
        sendShareReportError: true,
      };
    default:
      return state;
  }
}
