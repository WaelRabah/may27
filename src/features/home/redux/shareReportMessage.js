import {
  RETRIEVE_SHARE_REPORT_STATUS_BEGIN,
  RETRIEVE_SHARE_REPORT_STATUS_SUCCESS,
  RETRIEVE_SHARE_REPORT_STATUS_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retriveShareReportStatus(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_SHARE_REPORT_STATUS_BEGIN,
    });
    const url = `${apiEndPoints.shareReport.RETRIEVE_REPORT_STATUS}?topicId=${props.topicId}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_SHARE_REPORT_STATUS_SUCCESS,
            data: res.data.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_SHARE_REPORT_STATUS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_SHARE_REPORT_STATUS_BEGIN:
      return {
        ...state,
        retrieveShareReportStatusBegin: true,
        retrieveShareReportStatusSuccess: false,
        retrieveShareReportStatusError: false,
        shareReportStatus: false,
      };
    case RETRIEVE_SHARE_REPORT_STATUS_SUCCESS:
      return {
        ...state,
        retrieveShareReportStatusBegin: false,
        retrieveShareReportStatusSuccess: true,
        shareReportStatus: action.data.autoReport,
      };
    case RETRIEVE_SHARE_REPORT_STATUS_FAILURE:
      return {
        ...state,
        retrieveShareReportStatusBegin: false,
        retrieveShareReportStatusError: true,
      };
    default:
      return state;
  }
}
