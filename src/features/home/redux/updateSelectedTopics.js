import {
  UPDATE_SELECTED_TOPIC_BEGIN,
  UPDATE_SELECTED_TOPIC_SUCCESS,
  UPDATE_SELECTED_TOPIC_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateSelectedTopic(props = {}) {
  const { updatedTopic, prevTopic } = props;
  return dispatch => {
    dispatch({
      type: UPDATE_SELECTED_TOPIC_BEGIN,
      data: updatedTopic,
    });
    const url = `${apiEndPoints.topicsDropDown.UPDATE_TOPICDROPDOWN}?topicId=${updatedTopic.topicId}&status=true`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_SELECTED_TOPIC_SUCCESS,
            data: updatedTopic,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_SELECTED_TOPIC_FAILURE,
            data: prevTopic,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_SELECTED_TOPIC_BEGIN:
      return {
        ...state,
        updateSelectedTopicBegin: true,
        updateSelectedTopicSuccess: false,
        updateSelectedTopicFailure: false,
        headerTopicData: action.data,
      };
    case UPDATE_SELECTED_TOPIC_SUCCESS:
      return {
        ...state,
        updateSelectedTopicBegin: false,
        updateSelectedTopicSuccess: true,
        activeTopic: [action.data.topicId],
        headerTopicData: action.data,
      };
    case UPDATE_SELECTED_TOPIC_FAILURE:
      return {
        ...state,
        updateSelectedTopicBegin: false,
        headerTopicData: action.data,
        updateSelectedTopicFailure: true,
      };
    default:
      return state;
  }
}
