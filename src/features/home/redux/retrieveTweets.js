import {
    RETRIEVE_TWEETS_BEGIN,
    RETRIEVE_TWEETS_SUCCESS,
    RETRIEVE_TWEETS_FAILURE,
    FILTER_RETRIEVE_TWEETS_SUCCESS,
    FILTER_RETRIEVE_TWEETS_BEGIN,
    FILTER_RETRIEVE_TWEETS_FAILURE,
  } from './constants';
  import http from '../../../common/http';
  import { apiEndPoints } from '../../../common/globalConstants';
  
  export function retrieveAllTweets(props = {}) {
    return dispatch => {
      dispatch({
        type: RETRIEVE_TWEETS_BEGIN,
      });
      const url = `${apiEndPoints.tweets.RETRIEVE_TWEETS}?fromDate=${props.fromDate}&toDate=${
        props.toDate
      }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sentiment=${
        props.sentiments && props.sentiments.length ? props.sentiments.join(',').toLowerCase() : 'all'
      }`;
      const promise = new Promise((resolve, reject) => {
        const doRequest = http.get(url);
        doRequest.then(
          res => {
            dispatch({
              type: RETRIEVE_TWEETS_SUCCESS,
              data:
                res.status === 200
                  ? { count: res.data.count, data: res.data.data }
                  : { count: null, data: [] },
            });
            resolve(res);
          },
          err => {
            dispatch({
              type: RETRIEVE_TWEETS_FAILURE,
            });
            reject(err);
          },
        );
      });
      return promise;
    };
  }
  export function filterAllTweets(props = {}) {
    return dispatch => {
      dispatch({
        type: FILTER_RETRIEVE_TWEETS_BEGIN,
      });
      const url = `${apiEndPoints.tweets.RETRIEVE_TWEETS}?fromDate=${props.fromDate}&toDate=${
        props.toDate
      }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sentiment=${
        props.sentiments && props.sentiments.length ? props.sentiments.join(',').toLowerCase() : 'all'
      }`;
      const promise = new Promise((resolve, reject) => {
        const doRequest = http.get(url);
        doRequest.then(
          res => {
            dispatch({
              type: FILTER_RETRIEVE_TWEETS_SUCCESS,
              data:
                res.status === 200
                  ? { count: res.data.count, data: res.data.data }
                  : { count: null, data: [] },
            });
            resolve(res);
          },
          err => {
            dispatch({
              type: FILTER_RETRIEVE_TWEETS_FAILURE,
            });
            reject(err);
          },
        );
      });
      return promise;
    };
  }
  
  export function reducer(state, action) {
    switch (action.type) {
      case RETRIEVE_TWEETS_BEGIN:
        return {
          ...state,
          retrieveTweetsBegin: true,
          retrieveTweetsSuccess: false,
          retrieveTweetsFailure: false,
        };
      case RETRIEVE_TWEETS_SUCCESS:
        function tweetsStateAlreadyContainsThisReceivedData() {
          if (JSON.stringify(state.allTweets.data).includes(JSON.stringify(action.data.data).slice(0,-1))) {
            return true;
          }
          else {
            return false;
          }
        }
        let updatedTweetsData = [];
        if (state.allTweets.data && action.data.data && 
          (JSON.stringify(state.allTweets.data) !== JSON.stringify(action.data.data)) &&
          (!tweetsStateAlreadyContainsThisReceivedData())
          ) {
          updatedTweetsData = [...state.allTweets.data, ...action.data.data];
        } else if (action.data.data) {
          updatedTweetsData = action.data.data;
        }
        return {
          ...state,
          retrieveTweetsBegin: false,
          retrieveTweetsSuccess: true,
          allTweets: { data: updatedTweetsData, count: action.data.count },
        };
      case RETRIEVE_TWEETS_FAILURE:
        return {
          ...state,
          retrieveTweetsBegin: false,
          retrieveTweetsFailure: true,
        };
      case FILTER_RETRIEVE_TWEETS_BEGIN:
        return {
          ...state,
          retrieveTweetsBegin: true,
          retrieveTweetsSuccess: false,
          allTweets: {},
        };
      case FILTER_RETRIEVE_TWEETS_SUCCESS:
        return {
          ...state,
          retrieveTweetsBegin: false,
          retrieveTweetsSuccess: true,
          allTweets: { data: action.data.data, count: action.data.count },
        };
      case FILTER_RETRIEVE_TWEETS_FAILURE:
        return {
          ...state,
          retrieveTweetsBegin: false,
          retrieveTweetsFailure: true,
        };
      default:
        return state;
    }
  }
  