import {
  RETRIEVE_CATEGORIES_BEGIN,
  RETRIEVE_CATEGORIES_SUCCESS,
  RETRIEVE_CATEGORIES_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveAllCategories(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_CATEGORIES_BEGIN,
    });
    const url = `${apiEndPoints.categories.RETRIEVE_CATEGORIES}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_CATEGORIES_SUCCESS,
            data: res.data.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_CATEGORIES_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_CATEGORIES_BEGIN:
      return {
        ...state,
        retrieveCategoriesBegin: true,
        retrieveCategoriesSuccess: false,
        retrieveCategoriesFailure: false,
        allCategories: [],
      };
    case RETRIEVE_CATEGORIES_SUCCESS:
      return {
        ...state,
        retrieveCategoriesBegin: false,
        retrieveCategoriesSuccess: true,
        allCategories: action.data,
      };
    case RETRIEVE_CATEGORIES_FAILURE:
      return {
        ...state,
        retrieveCategoriesBegin: false,
        retrieveCategoriesFailure: true,
      };
    default:
      return state;
  }
}
