import React, { Component } from 'react';
import { Tweet } from './Tweet.js';
import PropTypes from 'prop-types';
import {
    Loader,
    NoTopic
} from '../../shared-components';
import { Link } from 'react-router-dom';

class TweetsTab extends Component {
    static propTypes = {
        tweets: PropTypes.object.isRequired,
        loadMoreTweets: PropTypes.func.isRequired,
        isTweetsLoading: PropTypes.bool.isRequired,
        cookies: PropTypes.object.isRequired,
        updateTweetsRelevant: PropTypes.func.isRequired,
        retrieveCategoriesBegin: PropTypes.bool.isRequired,
        isInitialLoadOfTweetsForThisTopic: PropTypes.bool.isRequired,
        topicId: PropTypes.oneOfType([
            PropTypes.array.isRequired,
            PropTypes.object.isRequired
        ])
    };

  constructor(props) {
    super(props);
    this.state = {
        tweets: {
            'data': [],
            count: 0
        },
        isInitialLoadOfTweetsForThisTopic: false,
        isTweetsLoading: false,
    };
  }

  componentDidMount() {
    this._setTweetsToStateIfNewTweets(this.props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this._setTweetsToStateIfNewTweets(nextProps);
  }

  setTweetsRelevant = tweetId => {
    const { updateTweetsRelevant } = this.props;
    updateTweetsRelevant(tweetId);
  };


  _setTweetsToStateIfNewTweets(nextProps) {
    let updatedState = {};
    if (nextProps.isInitialLoadOfTweetsForThisTopic != this.state.isInitialLoadOfTweetsForThisTopic) {
      updatedState['isInitialLoadOfTweetsForThisTopic'] = nextProps.isInitialLoadOfTweetsForThisTopic;
    }
    if (
      Object.keys(nextProps.tweets).length &&
      JSON.stringify(this.state.tweets) !== JSON.stringify(nextProps.tweets)
    ) {
      if (Array.isArray(nextProps.tweets.data)) {
        updatedState['tweets'] = nextProps.tweets;
      }
    }
    if (nextProps.isTweetsLoading !== this.state.isTweetsLoading) {
      updatedState['isTweetsLoading'] = nextProps.isTweetsLoading;
    }

    if (Object.keys(updatedState).length > 0) {
      this.setState(updatedState);
    }
  }

  render() {
    const { topicId } = this.props;
    const { tweets, isTweetsLoading, isInitialLoadOfTweetsForThisTopic } = this.state;
    return (
        <div className={`feed-comments`}>
        {topicId && topicId.length > 0 ? (
          <React.Fragment>
            {isInitialLoadOfTweetsForThisTopic && <Loader />}
            {!isInitialLoadOfTweetsForThisTopic && tweets.data.length > 0 &&
              tweets.data.map((tweet, index) => (
                <Tweet
                  cookies={this.props.cookies.cookies}
                  userTweet={tweet}
                  key={index}
                  updateRelevance={e => this.setTweetsRelevant(e)}
                />
              ))}
            {!isTweetsLoading && !isInitialLoadOfTweetsForThisTopic && !tweets.data.length && (
              <div className="no-feed-comment-data">
                <NoTopic
                  title="No Articles available"
                  message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                  icons={['exclamation-circle']}
                >
                  <button className="btn-no-data" onClick={() => this.showContactUs()}>
                    CONTACT US
                  </button>
                </NoTopic>
              </div>
            )}
          </React.Fragment>
        ) : (
          <NoTopic
            title="It’s Empty"
            message="Add topics and the related comments will appear here."
            icons={['comments']}
          >
            <Link to="/dashboard/topics">
              <button className="btn-add-topic">ADD TOPIC</button>
            </Link>
          </NoTopic>
        )}
        {!isInitialLoadOfTweetsForThisTopic && tweets['count'] && tweets['count'] > tweets['data'].length && (
          <button
            type="button"
            className={`btn btn-loadmore ${isTweetsLoading ? 'disabled' : ''}`}
            onClick={() => this.props.loadMoreTweets()}
          >
            {isTweetsLoading ? (
              <React.Fragment>
                <i className="la la-spinner la-spin" /> Loading
              </React.Fragment>
            ) : (
              'LOAD MORE'
            )}
          </button>
        )}
      </div>
    );
  }
}

export default TweetsTab;
