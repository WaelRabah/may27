import React, { useState, useEffect, Component } from 'react';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga'

import moment from 'moment';
import { socialIcons } from '../../../styles/icons';

export class Headline extends Component {
  static propTypes = {
    userHeadline: PropTypes.object.isRequired,
    updateRelevance: PropTypes.func.isRequired,
    cookies: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      userHeadline: {
        headline: "",
        source: "",
        sentiment: {
          category: ""
        },
        emotion: {
          category: ""
        }
      },
      updateRelevance: {},
      isSeeMore: false
    };
  }

  componentDidMount() {
    const userHeadline = this.props.userHeadline;
    this.setState({
      userHeadline: userHeadline
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      userHeadline: nextProps.userHeadline,
      updateRelevance: nextProps.updateRelevance
    });
  }

  setRelevance(headlineId, currentStatus) {
    const { userHeadline, updateRelevance } = this.state;
    if (userHeadline.relevance !== currentStatus) {
      userHeadline.relevance = currentStatus;
      updateRelevance({ headlineId, relevance: currentStatus });
    }
    return false;
  }

  toggleSeeMore = isSeeMore => {
    ReactGA.event(
      {
        category : 'Headlines' ,
        action : `User with id ${this.props.cookies.userMeta} was redirected to see more about ${this.props.userHeadline} from ${this.state.userHeadline.source}`
      }
    )
    this.setState({isSeeMore});
  }

  _getHeadlineCSSName = () => {
    const {userHeadline} = this.state;
    return userHeadline.source.toLowerCase().replace(/\s+/g, '_');;
  }

  render() {
    const { 
      userHeadline,
      isSeeMore,
    } = this.state;
    const headlineCSSName = this._getHeadlineCSSName();
    return (
        <div className="comment-card">
          <div className="header">
            <div className="category">
              <div
                className={`social-button card-header-button ${headlineCSSName}`}
              >
                <span
                  dangerouslySetInnerHTML={{
                    __html: socialIcons[headlineCSSName],
                  }}
                />
              </div>
              <div className="card-header-button">
                {userHeadline.sentiment.category && (
                  <button className={`btn-positive ${userHeadline.sentiment.category}`}>
                    {userHeadline.sentiment.category}
                  </button>
                )}
              </div>
              <div className="card-header-button">
                {userHeadline.emotion.category !== 'no specific emotion' &&
                  userHeadline.emotion.category && (
                    <div className="card-header-button">
                      {userHeadline.emotion.category && (
                        <button className={`btn-emotion ${userHeadline.emotion.category}`}>
                          {userHeadline.emotion.category}
                        </button>
                      )}
                    </div>
                  )}
              </div>
            </div>
            <div className="card-header-right w-50">
              <label>{moment(userHeadline.datetime).format('DD MMM YYYY hh:mm A')}</label>
            </div>
          </div>
          <div className="headline-content">
            <a href={userHeadline.sourceLink} target="_blank">
              <p className={`${!isSeeMore ? 'shrink-text-view' : ''}`}>{userHeadline.headline}</p>
            </a>
            {userHeadline.headline.length > 89 && !isSeeMore ? (
              <span className="see-more-btn" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                <u>See More</u>
              </span>
            ) : (
              userHeadline.headline.length > 89 &&
              isSeeMore && (
                <span className="see-more-btn less" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                  See Less
                </span>
              )
            )}
          </div>
          <div className="comment-action-btn">
            <button
              className={`btn-relevent ${userHeadline.relevance ? 'active' : ''}`}
              onClick={() => this.setRelevance(userHeadline.headlineId, true)}
            >
              Relevant
            </button>
            <button
              className={`btn-relevent ${!userHeadline.relevance ? 'active' : ''}`}
              onClick={() => this.setRelevance(userHeadline.headlineId, false)}
            >
              Irrelevant
            </button>
          </div>
        </div>
      )
  }
};

export default Headline;
