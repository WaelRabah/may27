import React, { useState, useEffect, Component } from 'react';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga'

import moment from 'moment';
import { socialIcons } from '../../../styles/icons';

export class Tweet extends Component {
  static propTypes = {
    userTweet: PropTypes.object.isRequired,
    updateRelevance: PropTypes.func.isRequired,
    cookies: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      userTweet: {
        tweet: "",
        source: "",
        sentiment: {
          category: ""
        },
        emotion: {
          category: ""
        }
      },
      updateRelevance: {},
      isSeeMore: false
    };
  }

  componentDidMount() {
    const userTweet = this.props.userTweet;
    this.setState({
      userTweet: userTweet
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      userTweet: nextProps.userTweet,
      updateRelevance: nextProps.updateRelevance
    });
  }

  setRelevance(tweetId, currentStatus) {
    const { userTweet, updateRelevance } = this.state;
    if (userTweet.relevance !== currentStatus) {
      userTweet.relevance = currentStatus;
      updateRelevance({ tweetId, relevance: currentStatus });
    }
    return false;
  }

  toggleSeeMore = isSeeMore => {
    ReactGA.event(
      {
        category : 'Tweets' ,
        action : `User with id ${this.props.cookies.userMeta} was redirected to see more about ${this.props.userTweet} from ${this.state.userTweet.source}`
      }
    )
    this.setState({isSeeMore});
  }

  _getTweetCSSName = () => {
    const {userTweet} = this.state;
    return userTweet.source.toLowerCase().replace(/\s+/g, '_');;
  }

  render() {
    const { 
      userTweet,
      isSeeMore,
    } = this.state;
    const tweetCSSName = this._getTweetCSSName();
    return (
        <div className="comment-card">
          <div className="header">
            <div className="category">
              <div
                className={`social-button card-header-button ${tweetCSSName}`}
              >
                <span
                  dangerouslySetInnerHTML={{
                    __html: socialIcons[tweetCSSName],
                  }}
                />
              </div>
              <div className="card-header-button">
                {userTweet.sentiment.category && (
                  <button className={`btn-positive ${userTweet.sentiment.category}`}>
                    {userTweet.sentiment.category}
                  </button>
                )}
              </div>
              <div className="card-header-button">
                {userTweet.emotion.category !== 'no specific emotion' &&
                  userTweet.emotion.category && (
                    <div className="card-header-button">
                      {userTweet.emotion.category && (
                        <button className={`btn-emotion ${userTweet.emotion.category}`}>
                          {userTweet.emotion.category}
                        </button>
                      )}
                    </div>
                  )}
              </div>
            </div>
            <div className="card-header-right w-50">
              <label>{moment(userTweet.datetime).format('DD MMM YYYY hh:mm A')}</label>
            </div>
          </div>
          <div className="content">
            <a href={userTweet.sourceLink} target="_blank">
              <p className={`${!isSeeMore ? 'shrink-text-view' : ''}`}>{userTweet.tweet}</p>
            </a>
            {userTweet.tweet.length > 135 && !isSeeMore ? (
              <span className="see-more-btn" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                <u>See More</u>
              </span>
            ) : (
              userTweet.tweet.length > 135 &&
              isSeeMore && (
                <span className="see-more-btn less" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                  See Less
                </span>
              )
            )}
          </div>
          <div className="comment-action-btn">
            <button
              className={`btn-relevent ${userTweet.relevance ? 'active' : ''}`}
              onClick={() => this.setRelevance(userTweet.tweetId, true)}
            >
              Relevant
            </button>
            <button
              className={`btn-relevent ${!userTweet.relevance ? 'active' : ''}`}
              onClick={() => this.setRelevance(userTweet.tweetId, false)}
            >
              Irrelevant
            </button>
          </div>
        </div>
      )
  }
};

export default Tweet;
