import React, { Component } from 'react';
import { Modal } from '../../shared-components';
import { TagsInput } from '../../shared-components';

const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
class ShareReportModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: {},
      emailList: [],
      isLoading: false,
    };
  }

  selectedTags = value => {
    let { error } = this.state;
    if (value.length) {
      value.forEach(email => {
        if (!reg.test(email)) {
          error['email'] = true;
        }
      });
    }
    this.setState({ emailList: value, error });
  };

  validateEmail = status => {
    let { error } = this.state;
    if (status) {
      error['email'] = true;
      this.setState({
        error,
      });
    } else {
      this.setState({
        error: {},
      });
    }
  };

  setModalActive = () => {
    let { shareData } = this.state;
    this.setState(prevState => {
      return {
        isShareModalActive: !prevState.isShareModalActive,
      };
    });
  };

  sendReportSubmit = () => {
    const { emailList } = this.state;
    if (emailList.length) {
      this.setState({
        isLoading: true,
      });
      this.props.sendReportFile(emailList);
    } else {
      let { error } = this.state;
      error['email'] = true;
      this.setState({
        error,
      });
    }
  };

  componentDidMount() {
    document.body.classList.add('modal-open');
  }

  componentWillUnmount() {
    document.body.classList.remove('modal-open');
  }

  render() {
    const { toggleShareReportModal, show, shareReportStatus, isReportSending } = this.props;
    const { emailList, error, isLoading } = this.state;
    return (
      <Modal handleClick={() => toggleShareReportModal(false)}>
        <div className={`modal share-report-modal ${show ? 'modal-show' : 'modal-hide'}`}>
          {show && (
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">Share Report</h4>
                  <div onClick={() => toggleShareReportModal(false)}>
                    <i
                      className="la la-close modal-close"
                      onClick={() => toggleShareReportModal(false)}
                    />
                  </div>
                </div>
                <div className="modal-body">
                  <div className="share-report">
                    <p>You may share a PDF version of this dashboard via email.</p>
                    <form>
                      <div className="form-group d-flex flex-row">
                        <label className="col-form-label">To</label>
                        <div
                          className={`add-email-container ${
                            error && error.email ? 'input-error' : ''
                          }`}
                        >
                          <TagsInput
                            placeholder="Eg: user1@email.com, user2@email.com, user3@email.com"
                            selectedTags={e => this.selectedTags(e)}
                            tagData={emailList}
                            type="email"
                            emailError={e => this.validateEmail(e)}
                          />
                          {error && error.email && (
                            <span className="instruction-msg error">
                              Please enter a valid email.
                            </span>
                          )}
                        </div>
                      </div>
                    </form>
                    {shareReportStatus && (
                      <p>Note: An auto-report is currently configured for this topic.</p>
                    )}
                  </div>
                </div>
                <div className="modal-footer">
                  <div className="report-action">
                    <button
                      className={`btn ${isLoading && isReportSending ? 'disabled' : ''}`}
                      onClick={() => toggleShareReportModal(false)}
                    >
                      CANCEL
                    </button>
                    <button
                      className={`btn btn-submit ${isLoading && isReportSending ? 'disabled' : ''}`}
                      onClick={() => this.sendReportSubmit()}
                    >
                      {isLoading && isReportSending ? (
                        <React.Fragment>
                          <i className="la la-spinner la-spin" /> SENDING..
                        </React.Fragment>
                      ) : (
                        'SEND'
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </Modal>
    );
  }
}

export default ShareReportModal;
