export const chartIcons = {
  sentiments: 'square-o',
  emotions: 'square-o',
  areaspline: 'area-chart',
  line: 'area-chart',
  donut: 'pie-chart',
  map: 'map',
  wordcloud: 'cloud',
  bar: 'bar-chart-o',
  spider: 'connectdevelop',
  column: 'bar-chart-o',
  sourcespercentcolumn: 'bar-chart-o'
};
export const chartIds = [
  100,
  101,
  102,
  103,
  104,
  105,
  106,
  107,
  108,
  109,
  110,
  111,
  112,
  113,
  114,
  115,
  116,
  117,
  118,
  119,
  120,
  121,
  122,
  123
];
export const growing_table = [
  {
    id: '1',
    data: '123,456',
  },
  {
    id: '2',
    data: '123,456',
  },
  {
    id: '3',
    data: '123,456',
  },
  {
    id: '4',
    data: '123,456',
  },
  {
    id: '5',
    data: '123,456',
  },
  {
    id: '6',
    data: '123,456',
  },
  {
    id: '7',
    data: '123,456',
  },
  {
    id: '8',
    data: '123,456',
  },
  {
    id: '9',
    data: '123,456',
  },
];

export const resource_table = [
  {
    id: '1',
    data: '123,456',
  },
  {
    id: '2',
    data: '123,456',
  },
  {
    id: '3',
    data: '123,456',
  },
  {
    id: '4',
    data: '123,456',
  },
  {
    id: '5',
    data: '123,456',
  },
  {
    id: '6',
    data: '123,456',
  },
  {
    id: '7',
    data: '123,456',
  },
  {
    id: '8',
    data: '123,456',
  },
  {
    id: '9',
    data: '123,456',
  },
];

export const index_table = [
  {
    id: '1',
    data: 'Total Mentions',
  },
  {
    id: '2',
    data: 'Positive Mentions',
  },
  {
    id: '3',
    data: 'Negative Mentions',
  },
  {
    id: '4',
    data: 'Neutral Mentions',
  },
  {
    id: '5',
    data: 'Sentiment Index',
  },
  {
    id: '6',
    data: 'Joy Index',
  },
  {
    id: '7',
    data: 'Anger Index',
  },
  {
    id: '8',
    data: 'Fear Index',
  },
  {
    id: '9',
    data: 'Sad Index',
  },
];
