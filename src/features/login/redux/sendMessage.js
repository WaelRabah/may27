import { SEND_MESSAGE_BEGIN, SEND_MESSAGE_SUCCESS, SEND_MESSAGE_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function sendMessage(props = {}) {
  return dispatch => {
    dispatch({
      type: SEND_MESSAGE_BEGIN,
    });
    const url = `${apiEndPoints.contact.SEND_MESSAGE}`;
    const promise = new Promise((resolve, reject) => {
      const formData = new FormData();
      Object.keys(props).forEach(formItem => {
        formData.append(formItem, props[formItem]);
      });
      const doRequest = http.post(url, formData);
      doRequest.then(
        res => {
          dispatch({
            type: SEND_MESSAGE_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: SEND_MESSAGE_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case SEND_MESSAGE_BEGIN:
      return {
        ...state,
        sendMessageBegin: true,
        sendMessageSuccess: false,
        sendMessageFailure: false,
      };
    case SEND_MESSAGE_SUCCESS:
      return {
        sendMessageBegin: false,
        sendMessageSuccess: action.data.status === 200 ? true : false,
        sendMessageFailure: action.data.status !== 200 ? true : false,
      };
    case SEND_MESSAGE_FAILURE:
      return {
        ...state,
        sendMessageBegin: false,
        sendMessageFailure: true,
      };
    default:
      return state;
  }
}
