const initialState = {
  sendMessageBegin: false,
  sendMessageSuccess: false,
  sendMessageFailure: false,
};

export default initialState;
