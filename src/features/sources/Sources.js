import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import * as updateSource from '../home/redux/updateSourcesFilter';
import { toast } from 'react-toastify';
import { Loader } from '../shared-components';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { socialIcons } from '../../styles/icons';
import { GaContext } from '../../googleAnalyticsContext/GaContext';
import ReactGA from 'react-ga';
import { 
    FACEBOOK_APP_ID, 
    FACEBOOK_SOCIAL_ICOSNS_KEY, 
    FACEBOOK_CONNECT_SCOPE,
    FACEBOOK_CONNECTION_ERROR_MSG,
    FACEBOOK_CONNECTION_SUCCESS_MSG,
    FACEBOOK_DISCONNECT_SUCCESS_MSG
} from './redux/constants';
class Input extends Component {
  render() {
    return <input {...this.props} />;
  }
}
export class Sources extends Component {
  static propTypes = {
    sources: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      allSources: [],
      isSourceUpdating: false,
      isDeletingFacebookConnection: false,
      isUpdatingFacebookConnection: false,
      isCheckingFacebookConnection: false,
      isFacebookConnected: false
    };
  }


  handleResponseFromFacebookConnect = (response) => {
    const { updateFacebookConnection } = this.props.actions;
    if (response.id && response.accessToken) {
      toast.success(' Successfully connected to Facebook.');
      this.setState({
        isUpdatingFacebookConnection: true,
        facebookUserId: response.id,
        facebookAccessToken: response.accessToken
      }, () => {
          updateFacebookConnection({
            short_lived_api_token: response.accessToken
          });
      });
    }
    else {
      toast.error(FACEBOOK_CONNECTION_ERROR_MSG);
    }
  }

  handleDeleteFaceookConnection = () => {
    this.setState({
      isDeletingFacebookConnection: true
    }, () => {
      this.props.actions.deleteFacebookConnection();
    });
  }

  setSourcesStatus = sourceName => {
    const { allSources } = this.state;
    const updateSource = allSources.filter(source => source.sourceName === sourceName)[0];
    const updatedSource = allSources.map(source => {
      if (source.sourceName.toLowerCase() === sourceName.toLowerCase())
        {source.selected = !source.selected;
      if (source.selected)
      {
          ReactGA.event(
            {
              category :'source' ,
              action : `${source.sourceName} was selected by user with id ${this.props.cookies.cookies.userMeta}`
            }
          )
      }
      else 
      {
        ReactGA.event(
          {
            category :'source' ,
            action : `${source.sourceName} was unselected by user with id ${this.props.cookies.cookies.userMeta}`
          }
        )
      }
    }
      return source;
    });
    this.setState(
      {
        allSources: [...updatedSource],
        isSourceUpdating: true,
      },
      () => {
        this.props.actions.updateSelectedSourceStatus(updateSource);
      },
    );
  };

  navigateBack = () => {
    this.props.history.goBack();
  };
  static contextType =GaContext
  componentDidMount() {
    
    const {ReactGA} = this.context
    ReactGA.pageview('/dashboard/sources')
    const { retrieveSources,checkFacebookConnection } = this.props.actions;
    retrieveSources();
    this.setState({
      isCheckingFacebookConnection: true
    }, () =>{
      checkFacebookConnection();
    });
  }
  componentWillUnmount() {
    this.setState({
      allSources: [],
    });
  }

  UNSAFE_componentWillReceiveProps(props) {
    if (
      props.sources.allAvailableSources.length &&
      !this.state.isSourceUpdating &&
      JSON.stringify(props.sources.allAvailableSources) !== JSON.stringify(this.state.allSources)
    ) {
      props.actions.updateAvailableSources(props.sources.allAvailableSources);
      this.setState({ allSources: [...props.sources.allAvailableSources] });
    }
    if (props.sources.update_sources_success && this.state.isSourceUpdating) {
      toast.success(' Sources selection has been updated!');
      this.setState({
        isSourceUpdating: false,
      });
    }
    if (props.sources.update_sources_error && this.state.isSourceUpdating) {
      toast.error(' Sources selection has not been updated!');
      this.setState({
        isSourceUpdating: false,
      });
    }
    this._setFacebookConnectionStateIfConnectionUpdated(props);
    this._setFacebookConnectionStateIfConnectionDeleted(props);
    this._setFacebookConnectionStateIfConnectionChecked(props);
  }

 
   _setFacebookConnectionStateIfConnectionUpdated(props) {
     if (this.state.isUpdatingFacebookConnection && props.sources.update_facebook_connection_success) {
       this.setState({
         isUpdatingFacebookConnection: false,
         isFacebookConnected: true
       }, () => {
         toast.success(FACEBOOK_CONNECTION_SUCCESS_MSG);
       });
       }
     }
   
   _setFacebookConnectionStateIfConnectionDeleted(props) {
     if (this.state.isDeletingFacebookConnection && props.sources.delete_facebook_connection_success) {
       this.setState({
         isDeletingFacebookConnection: false,
         isFacebookConnected: false
       }, () => {
         toast.success(FACEBOOK_DISCONNECT_SUCCESS_MSG);
       });
     }
   }
 
   _setFacebookConnectionStateIfConnectionChecked(props) {
     if (this.state.isCheckingFacebookConnection && props.sources.check_facebook_connection_success) {
       this.setState({
         isCheckingFacebookConnection: false,
         isFacebookConnected: props.sources.check_facebook_connection_result
       });
     }
   }


  render() {
    let allSources = [];
    allSources.push({
      sourceType: 'Social Media',
      sourcesData: this.state.allSources.filter(
        source => source.sourceType.toLowerCase() === 'social media',
      ),
    });
    allSources.push({
      sourceType: 'News',
      sourcesData: this.state.allSources.filter(
        source => source.sourceType.toLowerCase() === 'news',
      ),
    });
    const {isFacebookConnected} = this.state;
    return (
      <div className="sources-container">
        <div className="main-container">
          {allSources.length > 0 &&
            allSources.map((sources, i) => (
              <div className="col-6 sub-container" key={i}>
                <div className="col-12 source-content">
                  <div className="col-9 source-inner-content">
                    <h6 className="sub-text">{sources.sourceType}</h6>
                    {!sources.sourcesData.length && <Loader />}
                    {sources.sourcesData.length > 0 &&
                      sources.sourcesData.map((sourceData, index) => (
                        <div className="select-item" key={index}>
                          <Input
                            className="styled-checkbox"
                            id={sourceData.sourceName}
                            type="checkbox"
                            onChange={() => this.setSourcesStatus(sourceData.sourceName)}
                            checked={sourceData.selected}
                            value={sourceData.sourceName}
                          />
                          <label htmlFor={sourceData.sourceName}>{sourceData.sourceName}</label>
                          {sourceData.sourceType !== 'news' && sourceData.sourceName !== "facebook" && (
                            <div className="">
                              <div className="connection-status">
                                <span className="manage-connection">
                                  <span className="connection-btn">(Connect)</span>
                                </span>
                              </div>
                            </div>
                          )}
                          {sourceData.sourceName === "facebook" && (
                            <div className="">
                              <div className="connection-status">
                                <span className="manage-connection">
                                    {isFacebookConnected ? (
                                        <span onClick={this.handleDeleteFaceookConnection} className="facebook-connection-btn">
                                          <span
                                            dangerouslySetInnerHTML={{
                                              __html: socialIcons[FACEBOOK_SOCIAL_ICOSNS_KEY]
                                            }}
                                          /> 
                                            (Disconnect)
                                          </span>
                                      ) : (
                                        <FacebookLogin
                                          appId={FACEBOOK_APP_ID}
                                          autoLoad={false}
                                          scope={FACEBOOK_CONNECT_SCOPE}
                                          callback={this.handleResponseFromFacebookConnect}
                                          render={renderProps => (
                                            <span  onClick={renderProps.onClick} className="facebook-connection-btn">
                                              <span
                                                dangerouslySetInnerHTML={{
                                                  __html: socialIcons[FACEBOOK_SOCIAL_ICOSNS_KEY]
                                                }}
                                              /> 
                                              (Connect)
                                            </span>
                                          )}
                                        />
                                      )}
                                  </span>
                                </div>
                              </div>
                            )}
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    sources: state.sources,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions, ...updateSource }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Sources);
