import { Sources } from './';

export default {
  path: '/dashboard/sources',
  name: 'Sources',
  childRoutes: [{ path: 'sources', name: 'Sources', component: Sources, isIndex: true }],
};
