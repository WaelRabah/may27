import { 
  UPDATE_FACEBOOK_CONNECTION_BEGIN,
  UPDATE_FACEBOOK_CONNECTION_SUCCESS,
  UPDATE_FACEBOOK_CONNECTION_FAILURE 
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateFacebookConnection(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_FACEBOOK_CONNECTION_BEGIN,
      data: props,
    });
    const postData = Object.keys(props).map(params => `${params}=${props[params]}`);
    const url = `${apiEndPoints.facebook_connection.UPDATE_CONNECTION}?${postData.join('&')}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.post(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_FACEBOOK_CONNECTION_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_FACEBOOK_CONNECTION_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_FACEBOOK_CONNECTION_BEGIN:
      return {
        ...state,
        update_facebook_connection_begin: true,
        update_facebook_connection_success: false,
        update_facebook_connection_error: false,
      };
    case UPDATE_FACEBOOK_CONNECTION_SUCCESS:
      return {
        ...state,
        update_facebook_connection_begin: false,
        update_facebook_connection_success: true
      };
    case UPDATE_FACEBOOK_CONNECTION_FAILURE:
      return {
        ...state,
        update_facebook_connection_begin: false,
        update_facebook_connection_error: true,
      };

    default:
      return state;
  }
}
