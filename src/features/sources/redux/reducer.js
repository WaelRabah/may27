import initialState from './initialState';
import { reducer as retrieveSources } from './retrieveSources';
import { reducer as updateSources } from './updateSources';
import { reducer as deleteFacebookConnection } from './deleteFacebookConnection';
import { reducer as checkFacebookConnection } from './checkFacebookConnection';
import { reducer as updateFacebookConnection } from './updateFacebookConnection';

const reducers = [
  retrieveSources,
  updateSources,
  deleteFacebookConnection,
  checkFacebookConnection,
  updateFacebookConnection
];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
