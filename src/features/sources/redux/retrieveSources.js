import {
  RETRIEVE_SOURCES_BEGIN,
  RETRIEVE_SOURCES_SUCCESS,
  RETRIEVE_SOURCES_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveSources(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_SOURCES_BEGIN,
    });
    const url = `${apiEndPoints.sources.RETRIEVE_SOURCES}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_SOURCES_SUCCESS,
            data: res.data.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_SOURCES_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_SOURCES_BEGIN:
      return {
        ...state,
        retrieve_sources_begin: true,
        retrieve_sources_success: false,
        retrieve_sources_error: false,
        allAvailableSources: [],
      };
    case RETRIEVE_SOURCES_SUCCESS:
      return {
        ...state,
        retrieve_sources_begin: false,
        retrieve_sources_success: true,
        allAvailableSources: action.data,
      };
    case RETRIEVE_SOURCES_FAILURE:
      return {
        ...state,
        retrieve_sources_begin: false,
        retrieve_topics_error: true,
      };

    default:
      return state;
  }
}
