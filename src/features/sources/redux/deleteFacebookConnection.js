import { DELETE_FACEBOOK_CONNECTION_BEGIN, DELETE_FACEBOOK_CONNECTION_SUCCESS, DELETE_FACEBOOK_CONNECTION_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function deleteFacebookConnection(props = {}) {
  return dispatch => {
    dispatch({
      type: DELETE_FACEBOOK_CONNECTION_BEGIN,
      data: props,
    });
    const url = `${apiEndPoints.facebook_connection.DELETE_CONNECTION}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.delete(url);
      doRequest.then(
        res => {
          dispatch({
            type: DELETE_FACEBOOK_CONNECTION_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: DELETE_FACEBOOK_CONNECTION_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case DELETE_FACEBOOK_CONNECTION_BEGIN:
      return {
        ...state,
        delete_facebook_connection_begin: true,
        delete_facebook_connection_success: false,
        delete_facebook_connection_error: false,
      };
    case DELETE_FACEBOOK_CONNECTION_SUCCESS:
      return {
        ...state,
        delete_facebook_connection_begin: false,
        delete_facebook_connection_success: true,
        delete_facebook_connection_error: false
      };
    case DELETE_FACEBOOK_CONNECTION_FAILURE:
      return {
        ...state,
        delete_facebook_connection_begin: false,
        delete_facebook_connection_success: false,
        delete_facebook_connection_error: true,
      };

    default:
      return state;
  }
}
