  import {
    CHECK_FACEBOOK_CONNECTION_BEGIN,
    CHECK_FACEBOOK_CONNECTION_SUCCESS,
    CHECK_FACEBOOK_CONNECTION_FAILURE,
  } from './constants';
  import http from '../../../common/http';
  import { apiEndPoints } from '../../../common/globalConstants';
  
  export function checkFacebookConnection(props = {}) {
    return dispatch => {
      dispatch({
        type: CHECK_FACEBOOK_CONNECTION_BEGIN,
      });
      const url = `${apiEndPoints.facebook_connection.CHECK_CONNECTION}`;
      const promise = new Promise((resolve, reject) => {
        const doRequest = http.get(url);
        doRequest.then(
          res => {
            dispatch({
              type: CHECK_FACEBOOK_CONNECTION_SUCCESS,
              data: res.data.data,
            });
            resolve(res);
          },
          err => {
            dispatch({
              type: CHECK_FACEBOOK_CONNECTION_FAILURE,
            });
            reject(err);
          },
        );
      });
  
      return promise;
    };
  }
  
  export function reducer(state, action) {
    switch (action.type) {
      case CHECK_FACEBOOK_CONNECTION_BEGIN:
        return {
          ...state,
          check_facebook_connection_begin: true,
          check_facebook_connection_success: false,
          check_facebook_connection_error: false,
        };
      case CHECK_FACEBOOK_CONNECTION_SUCCESS:
        return {
          ...state,
          check_facebook_connection_begin: false,
          check_facebook_connection_success: true,
          check_facebook_connection_result: action.data,
        };
      case CHECK_FACEBOOK_CONNECTION_FAILURE:
        return {
          ...state,
          check_facebook_connection_begin: false,
          check_facebook_connection_error: true,
        };
  
      default:
        return state;
    }
  }
  