const initialState = {
  retrieve_sources_begin: false,
  retrieve_sources_error: false,
  retrieve_sources_success: false,
  check_facebook_connection_begin: false,
  check_facebook_connection_error: false,
  check_facebook_connection_success: false,
  update_facebook_connection_begin: false,
  update_facebook_connection_error: false,
  update_facebook_connection_success: false,
  check_facebook_connection_result: null,
  allAvailableSources: [],
  update_sources_begin: false,
  update_sources_success: false,
  update_sources_error: false,
};

export default initialState;
