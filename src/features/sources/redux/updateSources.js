import { UPDATE_SOURCES_BEGIN, UPDATE_SOURCES_SUCCESS, UPDATE_SOURCES_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateSelectedSourceStatus(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_SOURCES_BEGIN,
      data: props,
    });
    const url = `${apiEndPoints.sources.UPDATE_SOURCES}?sourceName=${props.sourceName}&selected=${props.selected}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_SOURCES_SUCCESS,
            data: props,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_SOURCES_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_SOURCES_BEGIN:
      return {
        ...state,
        update_sources_begin: true,
        update_sources_success: false,
        update_sources_error: false,
      };
    case UPDATE_SOURCES_SUCCESS:
      const { allAvailableSources } = state;
      const updatedSources = allAvailableSources.map(source => {
        if (source.sourceName === action.data.sourceName) {
          source.selected = action.data.selected;
        }
        return source;
      });
      return {
        ...state,
        allAvailableSources: [...updatedSources],
        update_sources_begin: false,
        update_sources_success: true,
      };
    case UPDATE_SOURCES_FAILURE:
      return {
        ...state,
        update_sources_begin: false,
        update_sources_error: true,
      };

    default:
      return state;
  }
}
