import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

class Autocomplete extends Component {
  static propTypes = {
    suggestions: PropTypes.instanceOf(Array),
  };

  static defaultProps = {
    suggestions: [],
  };

  constructor(props) {
    super(props);
    this.suggestionList = React.createRef();
    this.state = {
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      tags: [],
      userInput: '',
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      nextProps.tagData &&
      nextProps.tagData.length &&
      JSON.stringify(nextProps.tagData) != JSON.stringify(this.state.tags)
    ) {
      this.setState({
        tags: nextProps.tagData,
      });
    }
  }

  removeTags = indexToRemove => {
    const { tags } = this.state;
    const updatedTags = tags.filter((_, index) => index !== indexToRemove);
    this.setState({ tags: updatedTags }, () => this.props.selectedTags(this.state.tags));
  };

  addTags = selectedLabel => {
    if (selectedLabel !== '') {
      this.setState(
        prevState => {
          return {
            tags: [...prevState.tags, selectedLabel],
          };
        },
        () => this.props.selectedTags(this.state.tags),
      );
    }
  };

  onBlur = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;
    if (!filteredSuggestions.length) {
      this.addTags(e.target.value);
      this.setState({
        activeSuggestion: 0,
        filteredSuggestions: [],
        showSuggestions: false,
        userInput: '',
      });
    }
  };

  onChange = e => {
    const { suggestions } = this.props;
    const userInput = e.currentTarget.value;
    const filteredSuggestions = suggestions.filter(
      suggestion => suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1,
    );

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value,
    });
  };

  onClick = e => {
    const value = e.currentTarget.innerText;
    e.target.value = '';
    this.setState(
      {
        activeSuggestion: 0,
        filteredSuggestions: [],
        showSuggestions: false,
        userInput: '',
      },
      () => {
        this.addTags(value);
      },
    );
  };

  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: '',
      });
      this.addTags(filteredSuggestions[activeSuggestion]);

      e.stopPropagation();
      e.preventDefault();
    } else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return false;
      }
      this.setState({ activeSuggestion: activeSuggestion - 1 });
    } else if (e.keyCode === 40) {
      if (activeSuggestion === filteredSuggestions.length - 1) {
        return false;
      }
      let elHeight = 0;
      let elOffset = activeSuggestion * elHeight;
      let scrollTop = this.suggestionList.current.scrollHeight;
      let viewport = scrollTop + this.suggestionList.current.offsetHeight;

      this.suggestionList.current.childNodes.forEach(el => {
        if (el.classList[0] === 'suggestion-active') {
          elHeight = el.offsetTop;
        }
      });
      if (elOffset < scrollTop || elOffset + elHeight > viewport) {
        this.suggestionList.current.scrollTo({ top: 0 });
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  onKeyUp = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    if (e.key === ',' && !filteredSuggestions.length) {
      this.addTags(e.target.value.replace(/,$/, ''));
      this.setState({
        activeSuggestion: 0,
        filteredSuggestions: [],
        showSuggestions: false,
        userInput: '',
      });
    }
  };

  render() {
    const {
      onBlur,
      onChange,
      onClick,
      onKeyDown,
      onKeyUp,
      state: { activeSuggestion, filteredSuggestions, showSuggestions, userInput },
    } = this;
    const { placeholder } = this.props;
    const { tags } = this.state;
    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions" ref={this.suggestionList}>
            {filteredSuggestions.map((suggestion, index) => {
              let className;
              if (index === activeSuggestion) {
                className = 'suggestion-active';
              }
              return (
                <li className={className} key={suggestion} onClick={onClick}>
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">{/* <em>No category found!</em> */}</div>
        );
      }
    }

    return (
      <Fragment>
        <div className="tags-input">
          {tags &&
            tags.map((tag, index) => (
              <span className="tag-item" key={index}>
                <span>{tag}</span>
                <i className="la la-times-circle-o" onClick={() => this.removeTags(index)} />
              </span>
            ))}
          <input
            type="text"
            onChange={onChange}
            onBlur={onBlur}
            onKeyDown={onKeyDown}
            onKeyUp={onKeyUp}
            value={userInput}
            placeholder={placeholder}
            className="auto-suggest-input"
          />
          {suggestionsListComponent}
        </div>
      </Fragment>
    );
  }
}

export default Autocomplete;
