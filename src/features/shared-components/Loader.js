import React from 'react';

const Loader = () => {
  return (
    <div className="loader-wrapper">
      <div className="wrapper">
        <div className="loader-1 center">
          <span></span>
        </div>
      </div>
    </div>
  );
};

export default Loader;
