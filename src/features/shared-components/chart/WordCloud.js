import React, { Component } from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import worldCloud from 'highcharts/modules/wordcloud';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import moment from 'moment';
import { chartColorCode } from './colorCodes';

worldCloud(Highcharts);

class WordCloud extends AbstractChart {
  constructor(props) {
    super(props);
    this.state = {
      chartData: [],
      source: [],
      topicId: null,
      mapDataFromProps: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      mapOptions: {
        chart: {
          zoomType: 'xy',
          panKey: 'shift',
          panning: true,
          pinchType: 'xy',
          type: 'wordcloud',
          backgroundColor: 'white',
          marginTop: 25,
          marginLeft: 15,
          marginRight: 15,
          marginBottom: 15,
          width: 370,
          height: 240,
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        credits: {
          enabled: false,
        },
      },
    };
  }

  reRenderChartOnChange = props => {
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: this.state.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { chartId, selectedTopicId } = this.props;
    if (
      JSON.stringify(newProps.WordCloud[chartId]) !== JSON.stringify(this.state.mapDataFromProps)
    ) {
      const { title } = this.props;
      const chartData = newProps.WordCloud[chartId];

      let { mapOptions } = this.state;
      const updatedChartData = chartData['word'].map((word, index) => {
        return {
          name: word.replace('_', ' '),
          weight: chartData['freq'][index],
          color: chartColorCode[title.split(' ')[1].toLowerCase()],
        };
      });
      mapOptions.title.text = title;
      mapOptions['series'] = [
        {
          data: updatedChartData,
          name: 'Frequency',
          style: {
            fontFamily: 'Arial',
          },
        },
      ];
      this.setState({
        chartData: mapOptions['series'],
        mapOptions: mapOptions,
        mapDataFromProps: newProps.WordCloud[chartId],
        source: newProps.source,
        topicId: selectedTopicId,
      });
    }

    if (this._shouldReRenderChartBasedOnNewProps(newProps)) {
      this.reRenderChartOnChange(newProps);
    }
  }

  render() {
    const { mapOptions, mapDataFromProps } = this.state;
    const deepCopyOfMapOptions = JSON.parse(JSON.stringify(mapOptions));
    return (
      <React.Fragment>
        {Object.keys(mapDataFromProps).length > 0 ? (
          <HighchartsReact
            highcharts={Highcharts}
            options={deepCopyOfMapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact highcharts={Highcharts} options={deepCopyOfMapOptions} />
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    WordCloud: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WordCloud);
