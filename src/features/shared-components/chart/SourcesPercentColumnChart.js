import React, { Component } from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsReact from 'highcharts-react-official';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import moment from 'moment';
import { chartColorCode, labelColor } from './colorCodes';

HighchartsMore(Highcharts);

class SourcesPercentColumnChart extends AbstractChart {
  constructor(props) {
    super(props);
    this.state = {
      mapDataFromProps: {},
      chartData: [],
      source: [],
      topicId: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      mapOptions: {
        chart: {
          type: 'column',
          backgroundColor: 'white',
          marginBottom: 30,
          marginLeft: 60,
          marginRight:20,
          width: 740,
          height: 240,
        },
        plotOptions: {
          column: {
            dataLabels: {
              enabled: true,
            },
          },
        },
        legend: {
          enabled: true,
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'top',
          y: 0,
          x: 0,
          itemStyle: {
            color: '#999999',
            fontWeight: 'normal',
            fontSize: '10px',
            textTransform: 'capitalize',
          },
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        tooltip: { 
          enabled: true,
          valueSuffix: "%",
          valueDecimals: 2
        },
        credits: {
          enabled: false,
        },
        xAxis: {
          labels: {
            rotation: 0,
            style: {
              color: '#999999',
              fontSize: '8px',
              lineHeight: '5px',
              textTransform: 'capitalize',
            },
          },
        },
        yAxis: {
          labels: {
            style: {
              fontSize: '8px',
            },
            y: 2,
          },

          tickInterval: 10,
          title: {
            text: 'Percent of Headlines',
            style: {
              fontSize: '8px'
            }
          },
        },
      },
    };
  }

  reRenderChartOnChange = props => {
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: this.state.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { chartId, selectedTopicId, source } = this.props;
    if (
      JSON.stringify(newProps.SourcesPercentColumnChartt[chartId]) !==
        JSON.stringify(this.state.mapDataFromProps) &&
      newProps.selectedTopicId != null
    ) {
      this.setState({ chartData: [] });
      const { title } = this.props;
      let { mapOptions } = this.state;
      const chartData = newProps.SourcesPercentColumnChartt[chartId];
      if (chartData && chartData.hasOwnProperty("data")) {
        let series_data  = chartData["data"];
        for (let i=0; i< series_data.length; i++) {
          const series_name = series_data[i]["name"];
          series_data[i]["color"] = chartColorCode[series_name.toLowerCase()];
          series_data[i]["dataLabels"] = {
            enabled: true,
            align: 'center',
            format: '{point.y:.0f}%',
            y: 4,
            style: {
              color:  series_data[i]["color"],
              fontSize: '5px',
              fontWeight: 'bold',
              textOutline: 0,
            },
          };
        }
        mapOptions["series"] = series_data;
      }
      mapOptions['xAxis']['categories'] = chartData["sources"];
      mapOptions['title']['text'] = this.props.title;
      this.setState({
        chartData,
        mapOptions,
        mapDataFromProps: this.props.SourcesPercentColumnChartt[chartId],
        source,
      });
    }

    if (this._shouldReRenderChartBasedOnNewProps(newProps)) {
      this.reRenderChartOnChange(newProps);
    }
  }

  render() {
    const { mapOptions, mapDataFromProps } = this.state;
    return (
      <React.Fragment>
        {Object.keys(mapDataFromProps).length > 0 ? (
          <HighchartsReact
            highcharts={Highcharts}
            options={mapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact highcharts={Highcharts} options={mapOptions} />
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    SourcesPercentColumnChartt: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SourcesPercentColumnChart);
