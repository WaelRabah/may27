import React, { Component } from 'react';
import moment from 'moment';

class AbstractChart extends Component {

    constructor(props) {
        super(props);
        const that = this;
        this.state = {
            chartData: [],
            tickInterval: [],
            topicId: [],
            mapDataFromProps: [],
            source: [],
            filterDateRange: {
                fromDate: moment().format('YYYY-MM-DD'),
                toDate: moment().format('YYYY-MM-DD'),
            },
        }
    }

  _shouldReRenderChartBasedOnNewProps(nextProps) {
    if (nextProps.selectedTopicId == null) {
      return false;
    }
    if (this._newDateRangePropsDifferFromCurrentStateAndProps(nextProps)) {
      return true;
    }
    else if (this._newSourcePropsDifferFromCurrentStateAndProps(nextProps)) {
      return true;
    }
    else if (this._newTopicIdPropsDifferFromCurrentStateAndProps(nextProps)) {
      return true;
    }
    else {
      return false;
    }
  }

  _newDateRangePropsDifferFromCurrentStateAndProps(nextProps) {
    if (
      (JSON.stringify(this.state.filterDateRange) !== JSON.stringify(nextProps.dateRangeFilterValue)) &&
      (JSON.stringify(this.props.dateRangeFilterValue) !== JSON.stringify(nextProps.dateRangeFilterValue))
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  _newSourcePropsDifferFromCurrentStateAndProps(nextProps) {
    if (
      JSON.stringify(nextProps.source) !== JSON.stringify(this.state.source) && 
      JSON.stringify(nextProps.source) !== JSON.stringify(this.props.source)
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  _newTopicIdPropsDifferFromCurrentStateAndProps(nextProps) {
    if (
      (JSON.stringify(this.state.topicId) !== JSON.stringify(nextProps.selectedTopicId)) && 
      (JSON.stringify(this.props.selectedTopicId) !== JSON.stringify(nextProps.selectedTopicId))
    ) {
      return true;
    }
    else {
      return false;
    }
  }
}

export default AbstractChart;