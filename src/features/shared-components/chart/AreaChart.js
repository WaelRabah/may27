import React from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import noData from 'highcharts/modules/no-data-to-display';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { chartColorCode } from './colorCodes';
import moment from 'moment';

const timeConstant = 24 * 3600 * 1000;
const axisPointLimit = 5;
noData(Highcharts);
class AreaChart extends AbstractChart {
  constructor(props) {
    super(props);
    const that = this;
    this.state = {
      chartData: [],
      tickInterval: [],
      topicId: [],
      mapDataFromProps: [],
      source: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      mapOptions: {
        chart: {
          type: 'areaspline',
          backgroundColor: 'white',
          marginBottom: 30,
          marginLeft: 60,
          marginRight: 20,
          marginTop: 45,
          height: 240,
        },
        // series: [],
        legend: {
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'top',
          y: -36,
          itemStyle: {
            color: '#999999',
            fontWeight: 'normal',
            fontSize: '10px',
            textTransform: 'capitalize',
          },
        },
        plotOptions: {
          series: {
            marker: {
              enabled: true,
            },
            point: {
              events: {
                click: function() {
                  that.filterCommentsAndFeeds({
                    sentiment: [this.series.name],
                    dateTime: moment.utc(this.category).format('YYYY-MM-DD'),
                  });
                },
              },
            },
            cursor: 'pointer',
          },
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        tooltip: { enabled: true },
        credits: {
          enabled: false,
        },
        xAxis: {
          type: 'datetime',
          dateTimeLabelFormats: {
            day: '%e %b',
            minute: '%I:%M %P',
            hour: '%I:%M %P',
          },
          startOfWeek: 0,
          startOnTick: false,
          endOnTick: false,
          labels: {
            style: {
              color: '#999999',
              fontSize: '8px',
              lineHeight: '5px',
              textTransform: 'capitalize',
            },
          },
        },
        yAxis: {
          labels: {
            style: {
              fontSize: '8px',
            },
            y: 2,
          },
          tickInterval: 1000,
          title: {
            text: 'Number Of Comments',
            style: {
              fontSize: '8px'
            }
          },
        },
      },
    };
  }

  filterCommentsAndFeeds = (props = {}) => {
    this.props.filterBySentiments(props);
  };

  parseAreaChart = chart => {
    let tickInterval = [];
    let indexOfTotal = -1;
    Object.keys(chart).map(function(key, index) {
      if (chart[key]['category'] === 'total') indexOfTotal = index;
      if (chart[key]['category'] !== 'total') {
        chart[key].data.map(function(value, keys, index) {
          chart[key]['name'] = chart[key]['category'];
          chart[key]['marker'] = {
            fillColor: 'transparent',
          };
          chart[key]['color'] = chartColorCode[chart[key]['category']];
          chart[key]['fillColor'] = {
            linearGradient: [0, 0, 0, 300],
            stops: [
              [
                0,
                Highcharts.Color(chartColorCode[chart[key]['category']])
                  .setOpacity(0.5)
                  .get('rgba'),
              ],
              [
                1,
                Highcharts.Color(chartColorCode[chart[key]['category']])
                  .setOpacity(0)
                  .get('rgba'),
              ],
            ],
          };
          chart[key].data[keys][0] = moment.utc(value[0]).valueOf();
          tickInterval.push(chart[key].data[keys][1]);
          return value;
        });
        return chart[key];
      }
    });
    if (indexOfTotal !== -1) chart.splice(indexOfTotal, 1);
    return { chart, tickInterval };
  };

  getTickInterval = dateTime => {
    const startDate = moment(dateTime.fromDate);
    const endDate = moment(dateTime.toDate);
    const diffDays = endDate.diff(startDate, 'days');
    if (diffDays === 0) {
      return timeConstant / 4;
    } else if (diffDays < 21) {
      return timeConstant;
    } else if (diffDays > 21 && diffDays < 90) {
      return timeConstant * 7;
    } else if (diffDays > 90 && diffDays < 180) {
      return timeConstant * 30;
    } else if (diffDays > 180 && diffDays < 365) {
      return timeConstant * 90;
    } else {
      return timeConstant * 180;
    }
  };

  reRenderChartOnChange = nextProps => {
    let { mapOptions } = this.state;
    const { retrieveSelectedChartData } = this.props.actions;
    mapOptions['xAxis']['tickInterval'] = this.getTickInterval(nextProps.dateRangeFilterValue);
    this.setState(
      {
        mapOptions,
        filterDateRange: nextProps.dateRangeFilterValue,
        source: nextProps.source,
        topicId: nextProps.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: this.state.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { chartId, title, selectedTopicId, source } = this.props;

    if (this._newMapOptionPropsDifferentFromCurrentStateAndProps(nextProps)) {
      this.setState({ chartData: [] });
      const chartData = this.parseAreaChart(
        JSON.parse(JSON.stringify(nextProps.AreaChart[chartId])),
      );
      let { mapOptions } = this.state;
      mapOptions['series'] = chartData.chart ? chartData.chart : [];
      mapOptions['title']['text'] = title;

      mapOptions['yAxis']['tickInterval'] = chartData.tickInterval.length
        ? Number(
            (Number(Math.max(...chartData.tickInterval).toString()[0]) + 1)
              .toString()
              .padEnd(
                Math.max(...chartData.tickInterval).toString()[0] === '9'
                  ? Math.max(...chartData.tickInterval).toString().length + 1
                  : Math.max(...chartData.tickInterval).toString().length,
                0,
              ) / axisPointLimit,
          )
        : 1000;
      mapOptions['yAxis']['max'] = Math.max(...chartData.tickInterval);
      this.setState({
        chartData,
        mapOptions,
        mapDataFromProps: nextProps.AreaChart[chartId],
        source,
      });
    }

    if (this._shouldReRenderChartBasedOnNewProps(nextProps)) {
      this.reRenderChartOnChange(nextProps);
    }
  }


  _newMapOptionPropsDifferentFromCurrentStateAndProps(nextProps) {
    const { chartId, title, selectedTopicId, source } = this.props;
    if (
      (JSON.stringify(nextProps.AreaChart[chartId]) !== JSON.stringify(this.state.mapDataFromProps))
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  render() {
    const { mapOptions } = this.state;
    return (
      <React.Fragment>
        {mapOptions['series'] && mapOptions['series'].length > 0 ? (
          <HighchartsReact
            highcharts={Highcharts}
            options={mapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact highcharts={Highcharts} options={mapOptions} />
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    AreaChart: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AreaChart);
