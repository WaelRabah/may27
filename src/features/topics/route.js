import { Topics } from './';

export default {
  path: '/dashboard/topics',
  name: 'Topics',
  childRoutes: [{ path: 'topics', name: 'Topics', component: Topics, isIndex: true },
  { path: 'addReport', name: 'addTopic', component: Topics, isIndex: true },
  { path: 'editReport', name: 'editTopic', component: Topics, isIndex: true } , 
  { path: 'deleteReport', name: 'deleteTopic', component: Topics, isIndex: true }
],
};
