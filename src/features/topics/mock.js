export const topics_table = [
  {
    id: '1',
    name: 'Growing Up in Singapore Towards Healthy Outcome (GUSTO)',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    topic: 'Health, Healthcare, Singapore, Grow',
    category: 'A*STAR Buzz, A*STAR News',
  },
  {
    id: '12',
    name: 'A*STAR',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    topic: 'A*STAR, Research, Science',
    category: 'A*STAR Buzz, A*STAR News',
  },
  {
    id: '13',
    name: 'National Day Rally Sentiments',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    topic: 'SUSS, Bursary, Education',
    category: 'A*STAR Buzz, A*STAR News',
  },
  {
    id: '14',
    name: 'Fast Fashion',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    topic: 'AFashion, Purchase Power, E-Commerce',
    category: 'A*STAR Buzz, A*STAR News',
  },
  {
    id: '15',
    name: 'Buzzfeed Video Reactions',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    topic: 'Food',
    category: 'A*STAR Buzz, A*STAR News',
  },
  {
    id: '16',
    name: 'Sentiments towards Salted Egg Yolk',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    topic: 'Food',
    category: 'A*STAR Buzz, A*STAR News',
  },
];
