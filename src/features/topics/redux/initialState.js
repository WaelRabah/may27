const initialState = {
  retrieveTopicsBegin: false,
  retrieveTopicsFailure: false,
  allTopics: [],

  createTopicsBegin: false,
  createTopicsSuccess: false,
  createTopicsFailure: false,
  createdTopicId: null,

  retrieveCategoriesBegin: true,
  retrieveCategoriesFailure: false,
  retrieveCategoriesSuccess: false,
  allCategories: [],

  updateTopicsSuccess: false,
  updateTopicsFailure: false,
  updateTopicsBegin: false,

  deleteTopicBegin: false,
  deleteTopicFailure: false,
  deleteTopicSuccess: false,
};

export default initialState;
