import initialState from './initialState';
import { reducer as retrieveReports } from './retrieveReports';
import { reducer as createReport } from './createReport';
import { reducer as updateReport } from './updateReport';
import { reducer as deleteReport } from './deleteReport';
import { reducer as retrieveAllTopicsDropDown } from './retrieveAllTopicsDropDown';
const reducers = [retrieveReports, createReport, updateReport, deleteReport, retrieveAllTopicsDropDown];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
