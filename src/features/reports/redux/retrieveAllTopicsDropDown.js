import {
  RETRIEVE_ALLTOPICSDROPDOWN_BEGIN,
  RETRIEVE_ALLTOPICSDROPDOWN_SUCCESS,
  RETRIEVE_ALLTOPICSDROPDOWN_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';
const allTopicsDropdownResultLimit = 10000;

export function retrieveAllTopicsDropdown(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_ALLTOPICSDROPDOWN_BEGIN,
    });
    const url = `${apiEndPoints.topicsDropDown.RETRIEVE_TOPICDROPDOWN}?limit=${allTopicsDropdownResultLimit}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_ALLTOPICSDROPDOWN_SUCCESS,
            data:
              res.data.status === 200
                ? res.data.data.map(topic => {
                    return {
                      ...topic,
                      status: eval(topic.status),
                    };
                  })
                : [],
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_ALLTOPICSDROPDOWN_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}


export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_ALLTOPICSDROPDOWN_BEGIN:
      return {
        ...state,
        retrieveAllTopicsdropdownBegin: true,
        retrieveAllTopicsdropdownSuccess: false,
        retrieveAllTopicsdropdownError: false,
      };
    case RETRIEVE_ALLTOPICSDROPDOWN_SUCCESS:
      return {
        ...state,
        retrieveAllTopicsdropdownBegin: false,
        retrieveAllTopicsdropdownSuccess: true,
        topicDropdown: action.data,
      };
    case RETRIEVE_ALLTOPICSDROPDOWN_FAILURE:
      return {
        ...state,
        retrieveAllTopicsdropdownBegin: false,
        retrieveAllTopicsdropdownError: true,
      };
    default:
      return state;
  }
}
