export const RETRIEVE_REPORTS_BEGIN = 'RETRIEVE_REPORTS_BEGIN';
export const RETRIEVE_REPORTS_SUCCESS = 'RETRIEVE_REPORTS_SUCCESS';
export const RETRIEVE_REPORTS_FAILURE = 'RETRIEVE_REPORTS_FAILURE';
export const CREATE_REPORT_BEGIN = 'CREATE_REPORT_BEGIN';
export const CREATE_REPORT_SUCCESS = 'CREATE_REPORT_SUCCESS';
export const CREATE_REPORT_FAILURE = 'CREATE_REPORT_FAILURE';
export const UPDATE_REPORT_BEGIN = 'UPDATE_REPORT_BEGIN';
export const UPDATE_REPORT_SUCCESS = 'UPDATE_REPORT_SUCCESS';
export const UPDATE_REPORT_FAILURE = 'UPDATE_REPORT_FAILURE';
export const DELETE_REPORTS_BEGIN = 'DELETE_REPORTS_BEGIN';
export const DELETE_REPORTS_SUCCESS = 'DELETE_REPORTS_SUCCESS';
export const DELETE_REPORTS_FAILURE = 'DELETE_REPORTS_FAILURE';
export const RETRIEVE_ACTIVE_TOPIC_SUCCESS = 'RETRIEVE_ACTIVE_TOPIC_SUCCESS';
export const RETRIEVE_ACTIVE_TOPIC_FAILURE = 'RETRIEVE_ACTIVE_TOPIC_FAILURE';
export const RETRIEVE_ALLTOPICSDROPDOWN_BEGIN = 'RETRIEVE_ALLTOPICSDROPDOWN_BEGIN';
export const RETRIEVE_ALLTOPICSDROPDOWN_SUCCESS = 'RETRIEVE_ALLTOPICSDROPDOWN_SUCCESS';
export const RETRIEVE_ALLTOPICSDROPDOWN_FAILURE = 'RETRIEVE_ALLTOPICSDROPDOWN_FAILURE';
export const SUNDAY = 'Sunday';
export const MONDAY = 'Monday';
export const TUESDAY = 'Tuesday';
export const WEDNESDAY = 'Wednesday';
export const THURSDAY = 'Thursday';
export const FRIDAY = 'Friday';
export const SATURDAY = 'Saturday';
export const WEEKDAYS = [
  { name: SUNDAY,    symbol: 'S', js_value: 0, db_value: 7, status: false },
  { name: MONDAY,    symbol: 'M', js_value: 1, db_value: 1, status: false },
  { name: TUESDAY,   symbol: 'T', js_value: 2, db_value: 2, status: false },
  { name: WEDNESDAY, symbol: 'W', js_value: 3, db_value: 3, status: false },
  { name: THURSDAY,  symbol: 'T', js_value: 4, db_value: 4, status: false },
  { name: FRIDAY,    symbol: 'F', js_value: 5, db_value: 5, status: false },
  { name: SATURDAY,  symbol: 'S', js_value: 6, db_value: 6, status: false },
];
let weekday_info_by_name = {};
let weekday_info_by_db_value = {};
let weekday_info_by_js_value = {};
for (let i=0;i<WEEKDAYS.length;i++) {
  let weekday = WEEKDAYS[i];
  weekday_info_by_name[weekday['name']] = weekday;
  weekday_info_by_db_value[weekday['db_value']] = weekday;
  weekday_info_by_js_value[weekday['js_value']] = weekday;
}
export const WEEKDAY_INFO_BY_NAME = weekday_info_by_name;
export const WEEKDAY_INFO_BY_DB_VALUE = weekday_info_by_db_value;
export const WEEKDAY_INFO_BY_JS_VALUE = weekday_info_by_js_value;
export const UNCATEGORIZED_CATEGORY_NAME = 'Uncategorized';
export const OTHER_CATEGORY_NAME = 'Others';
export const CATEGORIES_TO_PUT_AT_END = [
  UNCATEGORIZED_CATEGORY_NAME,
  OTHER_CATEGORY_NAME
];
export const REPORT_NAME_INSTRUCTION_MSG = 'Enter a report name';
export const MISSING_REPORT_NAME_ERROR_MSG = 'Enter a valid report name';
export const REPORT_TOPIC_INSTRUCTION_MSG = 'Select a topic';
export const MISSING_REPORT_TOPIC_ERROR_MSG = 'Select a valid topic';
export const REPORT_DESCRIPTION_INSTRUCTION_MSG = 'Maximum 150 characters';
export const MISSING_REPORT_DESCRIPTION_ERROR_MSG = 'Please enter a description';
