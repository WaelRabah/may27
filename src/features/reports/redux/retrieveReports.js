import {
  RETRIEVE_REPORTS_BEGIN,
  RETRIEVE_REPORTS_SUCCESS,
  RETRIEVE_REPORTS_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveReports(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_REPORTS_BEGIN,
    });
    const url = `${apiEndPoints.reports.RETRIEVE_REPORTS}?pageLimit=${props.pageLimit}&pageId=${props.pageId}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_REPORTS_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_REPORTS_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_REPORTS_BEGIN:
      return {
        ...state,
        retrieveReportsBegin: true,
        retrieveReportsError: false,
      };
    case RETRIEVE_REPORTS_SUCCESS:
      return {
        ...state,
        retrieveReportsBegin: false,
        allReports: action.data,
      };
    case RETRIEVE_REPORTS_FAILURE:
      return {
        retrieveReportsBegin: false,
        retrieveReportsError: true,
        ...state,
      };

    default:
      return state;
  }
}
