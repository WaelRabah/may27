import { Reports } from './';
import AddReport from   "./components/AddReport"
 
 
export default {
  path: '/dashboard/reports',
  name: 'Reports',
  childRoutes: [{ path: 'reports', name: 'Reports', component: Reports, isIndex: true },
  { path: 'addReport', name: 'addReport', component: Reports, isIndex: true },
  { path: 'editReport', name: 'editReport', component: Reports, isIndex: true } , 
  { path: 'deleteReport', name: 'deleteReport', component: Reports, isIndex: true }
     
],
};
