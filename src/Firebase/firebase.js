import app from 'firebase/app';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import config from '../common/environmentConfig';
class Firebase {
  constructor() {
    app.initializeApp(config.firebase);
    this.auth = app.auth();
  }
  createUser = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);
  signIn = (email, password) => this.auth.signInWithEmailAndPassword(email, password);
  signOut = () => this.auth.signOut();
  resetPassword = email => this.auth.sendPasswordResetEmail(email);
  changePassword = password => this.auth.currentUser.updatePassword(password);
  getCredentials = (email, password) => firebase.auth.EmailAuthProvider.credential(email, password);
  reAuthtenticate = (email, password) => {
    const credential = this.getCredentials(email, password);
    return this.auth.currentUser.reauthenticateWithCredential(credential);
  };
}

export default Firebase;
