import { App } from '../features/home';
import { PageNotFound } from '../features/common';
import homeRoute from '../features/home/route';
import commonRoute from '../features/common/route';
import _ from 'lodash';
import topicsRoute from '../features/topics/route';
import sourcesRoute from '../features/sources/route';
import reportsRoute from '../features/reports/route';
import loginRoute from '../features/login/route';
import { Login } from '../features/login';
const childRoutes = [homeRoute, commonRoute, topicsRoute, sourcesRoute, reportsRoute];

const routes = [
  {
    path: '/login',
    component: Login,
    childRoutes: [...loginRoute],
  },
  {
    path: '/',
    component: App,
    childRoutes: [
      ...childRoutes,
      { path: '*', name: 'Page not found', component: PageNotFound },
    ].filter(r => r.component || (r.childRoutes && r.childRoutes.length > 0)),
  },
];

function handleIndexRoute(route) {
  if (!route.childRoutes || !route.childRoutes.length) {
    return;
  }

  const indexRoute = _.find(route.childRoutes, child => child.isIndex);
  if (indexRoute) {
    const first = { ...indexRoute };
    first.path = '';
    first.exact = true;
    first.autoIndexRoute = true;
    route.childRoutes.unshift(first);
  }
  route.childRoutes.forEach(handleIndexRoute);
}

routes.forEach(handleIndexRoute);
export default routes;
