import axios from 'axios';
import Promise from 'promise';
import config from './environmentConfig';
import { toast } from 'react-toastify';
const env = process.env.NODE_ENV || 'development';
const http = axios.create({
  baseURL: config[env].BASE_URL,
});

http.interceptors.request.use(
  function(config) {
    return config;
  },
  function(error) {
    return Promise.resolve({ error });
  },
);
http.interceptors.response.use(
  response => {
    return response;
  },
  err => {
    return new Promise(function(resolve, reject) {
      toast.error('Something went wrong.');
      if (err.response && err.response.status === 401) {
        return false;
      }
      throw err;
    });
  },
);

export default http;
