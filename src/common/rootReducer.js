import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import homeReducer from '../features/home/redux/reducer';
import commonReducer from '../features/common/redux/reducer';
import topicsReducer from '../features/topics/redux/reducer';
import sourcesReducer from '../features/sources/redux/reducer';
import reportsReducer from '../features/reports/redux/reducer';
import chartReducer from '../features/shared-components/chart/redux/reducer';
import sendMessageReducer from '../features/login/redux/reducer';

import { loadingBarReducer } from 'react-redux-loading-bar';
const reducerMap = {
  router: routerReducer,
  home: homeReducer,
  common: commonReducer,
  topics: topicsReducer,
  sources: sourcesReducer,
  reports: reportsReducer,
  charts: chartReducer,
  loadingBar: loadingBarReducer,
  newUser: sendMessageReducer,
};

export default combineReducers(reducerMap);
