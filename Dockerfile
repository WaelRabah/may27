FROM ubuntu:18.04

# Install OS-level dependencies.
RUN apt-get update && apt-get install -y nodejs-dev node-gyp libssl1.0-dev
RUN apt-get update && apt-get install -y nodejs
RUN apt-get update && apt-get install -y npm
RUN apt-get update && apt-get install -y git

# Copy the source code into the image.
RUN mkdir -p /apps/social_listening_frontend
COPY ./ /apps/social_listening_frontend

# We have to remove this directory or it will cause "npm install to fail."
RUN rm -rf /apps/social_listening_frontend/node_modules

# Install the React dependencies.
WORKDIR /apps/social_listening_frontend
RUN npm install