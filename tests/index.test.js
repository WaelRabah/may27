// index.js should run without errors.
describe('index', () => {
  // Skip because some mocking or other issues need to be added for this test to work.
  it.skip('index.js has no error', () => {
    document.body.innerHTML = '<div id="root"></div>';
    require('../src/index');
  });
});
