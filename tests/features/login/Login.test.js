import React from 'react';
import { shallow } from 'enzyme';
import { Login } from '../../../src/features/login';

describe('login', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const renderedComponent = shallow(<Login />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});
