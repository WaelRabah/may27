import React from 'react';
import { shallow } from 'enzyme';
import AddTopic from '../../../../src/features/topics/components/AddTopic';

describe('shared-components/AddTopic', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<AddTopic />);
    expect(renderedComponent.find('.add-topics').length).toBe(1);
  });
});
