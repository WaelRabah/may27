import React from 'react';
import { shallow, render } from 'enzyme';
import { Topics } from '../../../src/features/topics/Topics';

  const setup = propOverrides => {
  const props = Object.assign(
    {
    actions: {},
    topics: {
      allCategories: [],
      allTopics:{
        data: [
          {
            "categories":["Aviation"],
            "description":"na",
            "excludeKeywords":["None"],
            "lastModified":"Mon, 13 Jan 2020 02:36:57 GMT",
            "status":true,
            "topicId":3,
            "topicKeywords":["SilkAir New"],
            "topicName":"Silk Air New"
          },
          {
            "categories":["Aviation"],
            "description":"",
            "excludeKeywords":[],
            "lastModified":"Mon, 06 Jan 2020 18:17:17 GMT",
            "status":false,
            "topicId":6,
            "topicKeywords":["airport","airports"],
            "topicName":"airports"
          },
          {
            "categories": ["Bank"],
            "description": "",
            "excludeKeywords": [],
            "lastModified": "Mon, 06 Jan 2020 18:28:01 GMT",
            "status": false,
            "topicId": 7,
            "topicKeywords": [
              "Standard Chartered"
            ],
            "topicName": "standard chartered"
          },
          {
            "categories": ["Bank"],
            "description": "",
            "excludeKeywords": [],
            "lastModified": "Mon, 04 Nov 2019 07:00:31 GMT",
            "status": false,
            "topicId": 15,
            "topicKeywords": [
              "DBS",
              " DBS Bank"
            ],
            "topicName": "dbs"
          },
          {
            "categories": ["Bank"],
            "description": "",
            "excludeKeywords": [],
            "lastModified": "Tue, 07 Jan 2020 14:47:32 GMT",
            "status": false,
            "topicId": 155,
            "topicKeywords": [
              "dwdw"
            ],
            "topicName": "ocbc"
          }
        ],
        count: 5,
        status: 200,
        message: "success"
      },
      createTopicsBegin: false,
      createTopicsFailure: false,
      createTopicsSuccess: false,
      createdTopicId: null,
      deleteTopicBegin: false,
      deleteTopicFailure: false,
      deleteTopicSuccess: false,
      retrieveCategoriesBegin: false,
      retrieveCategoriesFailure: false,
      retrieveCategoriesSuccess: false,
      retrieveTopicsBegin: false,
      retrieveTopicsFailure: false,
      updateTopicsBegin: false,
      updateTopicsFailure: false,
      updateTopicsSuccess: false
    }
  }, 
  propOverrides
  );
  
    const renderedComponent = shallow(<Topics {...props} />);
    renderedComponent.setState({pageLimit: 2});
    renderedComponent.setProps(props);

    return {
      props,
      renderedComponent
    }
  }


describe('topics/Topics', () => {
  it('renders node with correct class name', () => {
    const {renderedComponent } = setup();
    expect(renderedComponent.find('.topics-container').length).toBe(1);
  });

  it('paginations properly during the initial page load', () => {
    const {props, renderedComponent } = setup();
    const {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(3);
    expect(paginatedAndFilteredTopicsData.length).toBe(2);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe(props.topics.allTopics.data[0]["topicName"]);
    expect(paginatedAndFilteredTopicsData[1]["topicName"]).toBe(props.topics.allTopics.data[1]["topicName"]);
  });

  it('can move forward one page properly with no filtering.', () => {
    const {props, renderedComponent } = setup();
    renderedComponent.instance().handlePagination("next");
    const {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(2);
    expect(totalPage).toBe(3);
    expect(paginatedAndFilteredTopicsData.length).toBe(2);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe(props.topics.allTopics.data[2]["topicName"]);
    expect(paginatedAndFilteredTopicsData[1]["topicName"]).toBe(props.topics.allTopics.data[3]["topicName"]);
  });

  it('can move forward to the last page properly with no filtering.', () => {
    const {props, renderedComponent } = setup();
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("next");
    const {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(3);
    expect(totalPage).toBe(3);
    expect(paginatedAndFilteredTopicsData.length).toBe(1);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe(props.topics.allTopics.data[4]["topicName"]);
  });
  
  it('will not try to go forward in pages past the last page.', () => {
    const {props, renderedComponent } = setup();
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("next");
    const {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(3);
    expect(totalPage).toBe(3);
    expect(paginatedAndFilteredTopicsData.length).toBe(1);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe(props.topics.allTopics.data[4]["topicName"]);
  });

  it('can move backwards properly with no filtering.', () => {
    const {props, renderedComponent } = setup();
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("prev");
    const {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(2);
    expect(totalPage).toBe(3);
    expect(paginatedAndFilteredTopicsData.length).toBe(2);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe(props.topics.allTopics.data[2]["topicName"]);
    expect(paginatedAndFilteredTopicsData[1]["topicName"]).toBe(props.topics.allTopics.data[3]["topicName"]);
  });

  it('will not go backward in pages to before the first page', () => {
    const {props, renderedComponent } = setup();
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("prev");
    renderedComponent.instance().handlePagination("prev");
    renderedComponent.instance().handlePagination("prev");

    const {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(3);
    expect(paginatedAndFilteredTopicsData.length).toBe(2);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe(props.topics.allTopics.data[0]["topicName"]);
    expect(paginatedAndFilteredTopicsData[1]["topicName"]).toBe(props.topics.allTopics.data[1]["topicName"]);
  });

  it('can filter and unfilter properly based on topic name', () => {
    const {props, renderedComponent } = setup();
    let e = {"target": {"value": "Silk"}};
    renderedComponent.instance().onBlurForSearchInput(e);
    let {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(1);
    expect(paginatedAndFilteredTopicsData.length).toBe(1);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe("Silk Air New");
    
    e.target.value = "";
    renderedComponent.instance().onBlurForSearchInput(e);
    currentPage = renderedComponent.state()["currentPage"];
    totalPage   = renderedComponent.state()["totalPage"];
    paginatedAndFilteredTopicsData = renderedComponent.state()["paginatedAndFilteredTopicsData"];
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(3);
    expect(paginatedAndFilteredTopicsData.length).toBe(2);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe(props.topics.allTopics.data[0]["topicName"]);
    expect(paginatedAndFilteredTopicsData[1]["topicName"]).toBe(props.topics.allTopics.data[1]["topicName"]);
  });

  it('can filter based on category, including paging back and forth', () => {
    const {props, renderedComponent } = setup();
    let e = {"target": {"value": "Bank"}};
    renderedComponent.instance().onBlurForSearchInput(e);
    let {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(2);
    expect(paginatedAndFilteredTopicsData.length).toBe(2);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe("standard chartered");
    expect(paginatedAndFilteredTopicsData[1]["topicName"]).toBe("dbs");
    
    renderedComponent.instance().handlePagination("next");
    renderedComponent.instance().handlePagination("next");
    currentPage = renderedComponent.state()["currentPage"];
    totalPage   = renderedComponent.state()["totalPage"];
    paginatedAndFilteredTopicsData = renderedComponent.state()["paginatedAndFilteredTopicsData"];
    expect(currentPage).toBe(2);
    expect(totalPage).toBe(2);
    expect(paginatedAndFilteredTopicsData.length).toBe(1);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe("ocbc");

    renderedComponent.instance().handlePagination("prev");
    renderedComponent.instance().handlePagination("prev");
    currentPage = renderedComponent.state()["currentPage"];
    totalPage   = renderedComponent.state()["totalPage"];
    paginatedAndFilteredTopicsData = renderedComponent.state()["paginatedAndFilteredTopicsData"];
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(2);
    expect(paginatedAndFilteredTopicsData.length).toBe(2);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe("standard chartered");
    expect(paginatedAndFilteredTopicsData[1]["topicName"]).toBe("dbs");
  });

  it("can filter based on description", () => {
    const {props, renderedComponent } = setup();
    let e = {"target": {"value": "na"}};
    renderedComponent.instance().onBlurForSearchInput(e);
    let {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(1);
    expect(paginatedAndFilteredTopicsData.length).toBe(1);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe("Silk Air New");
  });

  it('can filter based on keyword', () => {
    const {props, renderedComponent } = setup();
    let e = {"target": {"value": "dwd"}};
    renderedComponent.instance().onBlurForSearchInput(e);
    let {
      currentPage,
      totalPage, 
      paginatedAndFilteredTopicsData
    } = renderedComponent.state();
    expect(currentPage).toBe(1);
    expect(totalPage).toBe(1);
    expect(paginatedAndFilteredTopicsData.length).toBe(1);
    expect(paginatedAndFilteredTopicsData[0]["topicName"]).toBe("ocbc");
  });

  it('filters in a case insensitive way', () => {
    const {props, renderedComponent } = setup();
    let e = {"target": {"value": "bank"}};
    renderedComponent.instance().onBlurForSearchInput(e);
    const lowercaseFilteredTopicsData = renderedComponent.state()["filteredTopicsData"];
    e.target.value = "BANK";
    renderedComponent.instance().onBlurForSearchInput(e);
    const uppercaseFilteredTopicsData = renderedComponent.state()["filteredTopicsData"];
    expect(lowercaseFilteredTopicsData.length > 0).toBe(true);
    expect(uppercaseFilteredTopicsData.length > 0).toBe(true);
    expect(JSON.stringify(lowercaseFilteredTopicsData) === JSON.stringify(uppercaseFilteredTopicsData)).toBe(true);
  });
});




