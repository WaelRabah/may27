import React from 'react';
import { shallow } from 'enzyme';
import { AutoComplete } from '../../../src/features/shared-components';

describe('shared-components/AutoComplete', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<AutoComplete />);
    expect(renderedComponent.find('.tags-input').length).toBe(1);
  });
});
