import React from 'react';
import { shallow } from 'enzyme';
import { BarChart } from '../../../../src/features/shared-components';
import configStore from '../../../../src/common/configStore';

describe('shared-components/BarChart', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const store = configStore();
    const renderedComponent = shallow(<BarChart store={store}  />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});