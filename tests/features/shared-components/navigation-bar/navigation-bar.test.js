import React from 'react';
import { shallow } from 'enzyme';
import { NavigationBar } from '../../../../src/features/shared-components';

describe('shared-components/navigation-bar', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<NavigationBar />);
    expect(renderedComponent.find('.side-nav-container').length).toBe(1);
  });
});
