import React from 'react';
import { shallow } from 'enzyme';
import { Loader } from '../../../src/features/shared-components';

describe('shared-components/Loader', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<Loader />);
    expect(renderedComponent.find('.loader-wrapper').length).toBe(1);
  });
});
