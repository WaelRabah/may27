import React from 'react';
import { shallow } from 'enzyme';
import SignOut from '../../../../src/features/shared-components/Header/signOut';

describe('shared-components/Header/SignOut', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const renderedComponent = shallow(<SignOut />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});
