import React from 'react';
import { shallow } from 'enzyme';
import { Calender } from '../../../../src/features/shared-components';

describe('shared-components/Calender', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const renderedComponent = shallow(<Calender />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});
