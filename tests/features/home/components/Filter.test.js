import React from 'react';
import { shallow } from 'enzyme';
import Filter from '../../../../src/features/home/components/Filter';

describe('home/components/Filter', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const props = {};
    const renderedComponent = shallow(<Filter {...props} />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});


