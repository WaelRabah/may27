import React from 'react';
import { shallow } from 'enzyme';
import Compare  from '../../../../src/features/home/components/Compare';

describe('home/components/Compare', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const props = {
      sourcesFilter: [
        {"selected":true,"sourceName":"facebook","sourceType":"social media"},
        {"selected":true,"sourceName":"reddit","sourceType":"social media"},
        {"selected":true,"sourceName":"twitter","sourceType":"social media"},
        {"selected":true,"sourceName":"channel news asia","sourceType":"news"},
        {"selected":true,"sourceName":"straits times","sourceType":"news"}
      ],
      activeTopicCompareData: [],
      topicDropdown: [],
      topicCompareData: []
    };
    const renderedComponent = shallow(
      <Compare {...props} />
    );
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });

  it("Pulls date range from localStorage (in order to be shared with the dashboard.", () => {
    const props = {
      sourcesFilter: [
        {"selected":true,"sourceName":"facebook","sourceType":"social media"},
        {"selected":true,"sourceName":"reddit","sourceType":"social media"},
        {"selected":true,"sourceName":"twitter","sourceType":"social media"},
        {"selected":true,"sourceName":"channel news asia","sourceType":"news"},
        {"selected":true,"sourceName":"straits times","sourceType":"news"}
      ],
      activeTopicCompareData: [],
      topicDropdown: [],
      topicCompareData: []
    };
    const expectedFromDate = "2019-08-01";
    const expectedToDate   = "2019-08-31";
    localStorage.setItem(
      "dashboardFilter",
      JSON.stringify({'dateRangeFilterValue': {fromDate: expectedFromDate, toDate: expectedToDate}})
    );
    const renderedComponent = shallow(
            <Compare {...props} />
    );
    const actualFromDate = renderedComponent.state()['dateRange']['fromDate'];
    const actualToDate   = renderedComponent.state()['dateRange']['toDate'];

    expect(expectedFromDate == actualFromDate).toBe(true);
    expect(expectedToDate == actualToDate).toBe(true);
  });
});
