import React from 'react';
import { shallow } from 'enzyme';
import CategorySelectionModal  from '../../../../src/features/home/components/CategorySelectionModal';

describe('home/components/CategorySelectionModal', () => {
  it('renders node with correct class name', () => {
    const props = {
      setCategorySelectionModalActive: false,
      show: false,
      isLoading: false
    };
    const renderedComponent = shallow(<CategorySelectionModal {...props} />);
    expect(renderedComponent.find('.category-selector').length).toBe(1);
  });
});
