import React from 'react';
import { shallow } from 'enzyme';
import Dashboard  from '../../../../src/features/home/components/Dashboard';

describe('home/components/Dashboard', () => {
  it('renders node with correct class name', () => {
    const props = {
      availableCharts: [],
      feeds: [],
      comments: [],
      isCommentsLoading: false,
      sourcesFilter: [
        {"selected":true,"sourceName":"facebook","sourceType":"social media"},
        {"selected":true,"sourceName":"reddit","sourceType":"social media"},
        {"selected":true,"sourceName":"twitter","sourceType":"social media"},
        {"selected":true,"sourceName":"channel news asia","sourceType":"news"},
        {"selected":true,"sourceName":"straits times","sourceType":"news"}
      ],
      isFeedsLoading: false,
      topicId: 0,
      updateSelectedCharts: false,
      isShareReportSending: false,
      retrieveCategoriesBegin: function(){},
      retrieveFeedsBegin: function(){},
      feedDetails: {}
    };
    const renderedComponent = shallow(<Dashboard {...props} />);
    expect(renderedComponent.find('.dashboard-container').length).toBe(1);
  });
});