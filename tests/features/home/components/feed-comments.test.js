import React from 'react';
import { shallow } from 'enzyme';
import FeedComments  from '../../../../src/features/home/components/feed-comments';

describe('home/components/FeedComments', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const props = {
      userComment: {},
      updateRelevance: function(){}
    };
    const renderedComponent = shallow(<FeedComments {...props} />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});