import React from 'react';
import { shallow } from 'enzyme';
import AddChartModal  from '../../../../src/features/home/components/AddChartModal';

describe('home/components/AddChartModal', () => {
  it('renders node with correct class name', () => {
    const props = {
      isAddChartActive: false,
      handleAddChart: function(){},
      availableCharts: [],
      updateAvailableChart: function(){}
    };
    const renderedComponent = shallow(<AddChartModal {...props} />);
    expect(renderedComponent.find('.add-chart-type').length).toBe(1);
  });
});

