import React from 'react';
import { shallow } from 'enzyme';
import ShareReportModal  from '../../../../src/features/home/components/ShareReportModal';

describe('home/components/SharedReportModal', () => {
  it('renders node with correct class name', () => {
    const props = {};
    const renderedComponent = shallow(<ShareReportModal {...props} />);
    expect(renderedComponent.find('.share-report-modal').length).toBe(1);
  });
});


