#!/usr/bin/python3

import json
import socket
import sys, os
import copy

CONFIG_DIR = os.path.dirname(os.path.realpath(__file__)) + "/../../config/social_listening"
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
INPUT_CONFIG_FILE_PATH = CONFIG_DIR + "/server-specific-config.json"
OUTPUT_ENV_FILE_PATH   = SCRIPT_DIR + "/../../.env"

class ReactEnvFileGenerator:
    def __init__(self):
        self._input_json = None
        self._config_json = None

    def generate_env_file(self):
        self._read_server_specific_config()
        self._build_config_json_based_to_default_values()
        self._update_config_json_based_on_hostname_if_possible()
        self._write_config_json_to_env_file()
        
    def _read_server_specific_config(self):
        with open(INPUT_CONFIG_FILE_PATH) as f:
            self._input_json = json.load(f)
    
    def _build_config_json_based_to_default_values(self):
        self._config_json = copy.deepcopy(self._input_json['default'])

    def _update_config_json_based_on_hostname_if_possible(self):
        hostname  = socket.gethostname()
        if hostname in self._input_json.keys():
            for key, value in self._input_json[hostname].items():
                self._config_json[key] = value

    def _write_config_json_to_env_file(self):
        with open(OUTPUT_ENV_FILE_PATH, 'w') as outfile:
            for key, value in self._config_json.items():
                outfile.write(key + "=" + value + "\n")
    

if __name__ == '__main__':
    ReactEnvFileGenerator().generate_env_file()